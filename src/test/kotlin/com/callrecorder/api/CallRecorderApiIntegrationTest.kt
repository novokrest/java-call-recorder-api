package com.callrecorder.api

import com.callrecorder.api.dto.*
import org.amshove.kluent.*
import org.testng.annotations.BeforeClass
import org.testng.annotations.Ignore
import org.testng.annotations.Test
import java.io.File
import java.time.OffsetDateTime

/**
 * Integration test for [CallRecorderApi]
 *
 * @author Konstantin Novokreshchenov (novokrest013@gmail.com)
 * @since 30.04.2019
 */
class CallRecorderApiIntegrationTest {

    private val api = CallRecorderApi()

    @BeforeClass
    fun beforeClass() {
        api.apiClient.isDebugging = true
        api.apiClient.isVerifyingSsl = false
    }

    private var testVerifyPhoneCode: String? = null
    private var testApiKey: String? = null
    private var testCreatedFileId: Long? = null
    private var testCreatedFolderId: Long? = null
    private var testCreatedMetaFileId: Long? = null

    @Test
    fun `should send request to register phone successfully`() {
        // given
        val request = RegisterPhoneRequest()
                .token(API_TOKEN)
                .phone(PHONE_NUMBER)

        // when
        val response: RegisterPhoneResponse = api.registerPhonePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.phone shouldBeEqualTo request.phone
        response.code.shouldNotBeEmpty()
        response.msg shouldBeEqualTo "Verification Code Sent"

        testVerifyPhoneCode = response.code
    }

    @Test(dependsOnMethods = ["should send request to register phone successfully"])
    fun `should send request to verify phone successfully`() {
        // given
        val request = VerifyPhoneRequest()
                .token(API_TOKEN)
                .phone(PHONE_NUMBER)
                .code(testVerifyPhoneCode)
                .mcc("300")
                .app(App.REC)
                .deviceType(DeviceType.IOS)
                .deviceToken(DEVICE_TOKEN)
                .deviceId(DEVICE_TOKEN)
                .timeZone("10")

        // when
        val response: VerifyPhoneResponse = api.verifyPhonePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.phone shouldBeEqualTo request.phone
        response.apiKey.shouldNotBeEmpty()
        response.msg shouldBeEqualTo "Phone Verified"

        testApiKey = response.apiKey
    }

    @Test(dependsOnMethods = ["should send request to verify phone successfully"])
    fun `should send request to get files successfully`() {
        // given
        val request = GetFilesRequest()
                .apiKey(testApiKey)
                .page("0")
                .folderId(0)
                .source(GetFilesRequest.SourceEnum.ALL)
                .pass("0")
                .reminder(false)
                .q("hello")
                .id(0)
                .op(GetFilesRequest.OpEnum.GREATER)

        // when
        val response: GetFilesResponse = api.getFilesPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.credits shouldEqualTo 0
        response.creditsTrans shouldEqualTo 0
        response.files.shouldBeEmpty()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to create file successfully`() {
        // given
        val file = File(javaClass.getResource("audio.mp3").toURI())

        val request = CreateFileRequest()
                .apiKey(testApiKey)
                .file(file)
                .data(CreateFileData()
                        .name("test-file")
                        .notes("test-notes")
                        .remindDays(10.toString())
                        .remindDate(OffsetDateTime.now()))

        // when
        val response: CreateFileResponse = api.createFilePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Successfully"
        response.id shouldBeGreaterThan 0

        testCreatedFileId = response.id
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create file successfully"
    ])
    fun `should send request to delete files successfully`() {
        // given
        val request = DeleteFilesRequest()
                .apiKey(testApiKey)
                .ids(listOf(testCreatedFileId, 123))

        // when
        val response: DeleteFilesResponse = api.deleteFilesPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Successfully"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to create folder successfully`() {
        // given
        val request = CreateFolderRequest()
                .apiKey(testApiKey)
                .name("test-folder")
                .pass(FOLDER_PASS)

        // when
        val response: CreateFolderResponse = api.createFolderPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Successfully"
        response.id shouldBeGreaterThan 0
        response.code.shouldNotBeNullOrBlank()

        testCreatedFolderId = response.id
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create folder successfully"
    ])
    fun `should send request to update folder successfully`() {
        // given
        val request = UpdateFolderRequest()
                .apiKey(testApiKey)
                .id(testCreatedFolderId)
                .name("test-folder-up")
                .pass(FOLDER_PASS)
                .isPrivate(false)

        // when
        val response: UpdateFolderResponse = api.updateFolderPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Folder Update"
        response.code.shouldNotBeNullOrBlank()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to verify folder pass successfully`() {
        // given
        val testFolderId = createTestFolder()
        val request = VerifyFolderPassRequest()
                .apiKey(testApiKey)
                .id(testFolderId)
                .pass(FOLDER_PASS)

        // when
        val response: VerifyFolderPassResponse = api.verifyFolderPassPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Password is Correct"
        response.code.shouldNotBeNullOrBlank()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create folder successfully"
    ])
    fun `should send request to delete folder successfully`() {
        // given
        val request = DeleteFolderRequest()
                .apiKey(testApiKey)
                .id(testCreatedFolderId)

        // when
        val response: DeleteFolderResponse = api.deleteFolderPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Successfully"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create folder successfully"
    ])
    fun `should send request to get folders successfully`() {
        // given
        val request = GetFoldersRequest()
                .apiKey(testApiKey)

        // when
        val response: GetFoldersResponse = api.getFoldersPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
        response.folders.shouldNotBeEmpty()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create file successfully"
    ])
    fun `should send request to update star successfully`() {
        // given
        val request = UpdateStarRequest()
                .apiKey(testApiKey)
                .id(testCreatedFileId)
                .type(UpdateStarRequest.TypeEnum.FILE)
                .star(true)

        // when
        val response: UpdateStarResponse = api.updateStarPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
        response.code shouldBeEqualTo "star_updated"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create file successfully"
    ])
    fun `should send request to clone file successfully`() {
        // given
        val request = CloneFileRequest()
                .apiKey(testApiKey)
                .id(testCreatedFileId)

        // when
        val response: CloneFileResponse = api.cloneFilePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
        response.code shouldBeEqualTo "file_cloned"
        response.id shouldBeGreaterThan 0
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to update profile image successfully`() {
        // given
        val img = File(javaClass.getResource("java.png").toURI())

        val request = UpdateProfileImgRequest()
                .apiKey(testApiKey)
                .file(img)

        // when
        val response: UpdateProfileImgResponse = api.updateProfileImgPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Profile Picture Updated"
        response.code shouldBeEqualTo "profile_pic_updated"
        response.file shouldEndWith ".png"
        response.path.shouldNotBeNullOrEmpty()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to update profile successfully`() {
        // given
        val request = UpdateProfileRequest()
                .apiKey(testApiKey)
                .data(UpdateProfileRequest.ProfileData()
                        .fName("testFName")
                        .lName("testLName")
                        .email("test@mail.com")
                        .isPublic(true)
                        .language("en_us"))

        // when
        val response: UpdateProfileResponse = api.updateProfilePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Profile Updated"
        response.code shouldBeEqualTo "profile_updated"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to get profile successfully`() {
        // given
        val request = GetProfileRequest()
                .apiKey(testApiKey)

        // when
        val response: GetProfileResponse = api.getProfilePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.code shouldBeEqualTo  "user_profile"
        response.profile.shouldNotBeNull()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create folder successfully"
    ])
    fun `should send request to update folder's order successfully`() {
        // given
        val request = UpdateOrderRequest()
                .apiKey(testApiKey)
                .folders(listOf(
                        UpdateOrderRequest.FolderOrder()
                                .id(testCreatedFolderId)
                                .orderId(12345)
                ))

        // when
        val response: UpdateOrderResponse = api.updateOrderPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Order Updated"
        response.code shouldContain "order_updated"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create file successfully",
        "should send request to delete files successfully"
    ])
    fun `should send request to recover file successfully`() {
        // given
        val request = RecoverFileRequest()
                .apiKey(testApiKey)
                .id(testCreatedFileId)

        // when
        val response: RecoverFileResponse = api.recoverFilePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Successfully Recovered"
        response.code shouldContain "file_recovered"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to update settings successfully`() {
        // given
        val request = UpdateSettingsRequest()
                .apiKey(testApiKey)
                .playBeep(PlayBeep.NO)
                .filesPermission(FilesPermission.PRIVATE)

        // when
        val response: UpdateSettingsResponse = api.updateSettingsPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
        response.code shouldContain "settings_updated"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to get settings successfully`() {
        // given
        val request = GetSettingsRequest()
                .apiKey(testApiKey)

        // when
        val response: GetSettingsResponse = api.getSettingsPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.app.shouldNotBeNull()
        response.credits.shouldNotBeNull()
        response.settings.playBeep.shouldNotBeNull()
        response.settings.filesPermission.shouldNotBeNull()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to get messages successfully`() {
        // given
        val request = GetMessagesRequest()
                .apiKey(testApiKey)

        // when
        val response: GetMessagesResponse = api.getMessagesPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msgs.shouldNotBeEmpty()
        response.msgs.forEach {
            it.id shouldBeGreaterThan 0
            it.title.shouldNotBeNull()
            it.body.shouldNotBeNull()
            it.time.shouldNotBeNull()
        }
    }

    @Ignore
    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to buy credits successfully`() {
        // given
        val request = BuyCreditsRequest()
                .apiKey(testApiKey)
                .amount(100)
                .receipt("test")
                .deviceType(DeviceType.IOS)
                .productId(1)

        // when
        val response: BuyCreditsResponse = api.buyCreditsPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to update device token successfully`() {
        // given
        val request = UpdateDeviceTokenRequest()
                .apiKey(testApiKey)
                .deviceToken(DEVICE_TOKEN)
                .deviceType(DeviceType.IOS)

        // when
        val response: UpdateDeviceTokenResponse = api.updateDeviceTokenPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to get translations successfully`() {
        // given
        val request = GetTranslationsRequest()
                .apiKey(testApiKey)
                .language("en_US")

        // when
        val response: GetTranslationsResponse = api.getTranslationsPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.translation.shouldNotBeEmpty()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to get languages successfully`() {
        // given
        val request = GetLanguagesRequest()
                .apiKey(testApiKey)

        // when
        val response: GetLanguagesResponse = api.getLanguagesPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg.shouldBeNull()
        response.languages.shouldNotBeEmpty()
        response.languages.forEach {
            it.code.shouldNotBeEmpty()
            it.name.shouldNotBeEmpty()
        }
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to get phones successfully`() {
        // given
        val request = GetPhonesRequest()
                .apiKey(testApiKey)

        // when
        val response: GetPhonesResponse = api.getPhonesPost(request)

        // then
        response.shouldNotBeEmpty()
        response.forEach {
            it.phoneNumber.shouldNotBeEmpty()
            it.number.shouldNotBeEmpty()
            it.prefix.shouldNotBeEmpty()
            it.friendlyName.shouldNotBeEmpty()
            it.flag.shouldNotBeEmpty()
            it.country.shouldNotBeEmpty()
        }
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to update user successfully`() {
        // given
        val request = UpdateUserRequest()
                .apiKey(testApiKey)
                .app(App.REC)
                .timezone("10")

        // when
        val response: UpdateUserResponse = api.updateUserPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg.shouldNotBeNull()
    }

    @Ignore
    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully"
    ])
    fun `should send request to notify user successfully`() {
        // given
        val request = NotifyUserRequest()
                .apiKey(testApiKey)
                .title("test-title")
                .body("test-body")
                .deviceType(DeviceType.IOS)

        // when
        val response: NotifyUserResponse = api.notifyUserPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg.shouldBeNull()
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create file successfully"
    ])
    fun `should send request to upload meta file successfully`() {
        // given
        val request = UploadMetaFileRequest()
                .apiKey(testApiKey)
                .file(File(javaClass.getResource("java.png").toURI()))
                .name("test-meta")
                .parentId(testCreatedFileId)

        // when
        val response: UploadMetaFileResponse = api.uploadMetaFilePost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "Success"
        response.id shouldBeGreaterThan 0
        response.parentId shouldEqualTo testCreatedFileId!!

        testCreatedMetaFileId = response.id
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to create file successfully",
        "should send request to upload meta file successfully"
    ])
    fun `should send request to get meta files successfully`() {
        // given
        val request = GetMetaFilesRequest()
                .apiKey(testApiKey)
                .parentId(testCreatedFileId)

        // when
        val response: GetMetaFilesResponse = api.getMetaFilesPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.metaFiles.shouldNotBeEmpty()
        response.metaFiles.map { it.id } shouldContain testCreatedMetaFileId!!
    }

    @Test(dependsOnMethods = [
        "should send request to register phone successfully",
        "should send request to verify phone successfully",
        "should send request to upload meta file successfully"
    ])
    fun `should send request to delete meta files successfully`() {
        // given
        val testFileId = createTestFile()
        val (testMetaFileId, testParentId) = createTestMetaFile(testFileId)

        val request = DeleteMetaFilesRequest()
                .apiKey(testApiKey)
                .ids(listOf(testMetaFileId))
                .parentId(testParentId)

        // when
        val response: DeleteMetaFilesResponse = api.deleteMetaFilesPost(request)

        // then
        response.status shouldBeEqualTo SUCCESS_STATUS
        response.msg shouldContain "deleted"
    }

    private fun createTestFile(): Long {
        val request = CreateFileRequest()
            .apiKey(testApiKey)
            .file(File(javaClass.getResource("audio.mp3").toURI()))
            .data(CreateFileData()
                .name("test-file")
                .notes("test-notes")
                .remindDays(10.toString())
                .remindDate(OffsetDateTime.now()))

        return api.createFilePost(request).id
    }

    private fun createTestMetaFile(fileId: Long): Pair<Long, Long> {
        val request = UploadMetaFileRequest()
            .apiKey(testApiKey)
            .file(File(javaClass.getResource("java.png").toURI()))
            .name("test-meta")
            .parentId(fileId)

        val response = api.uploadMetaFilePost(request)
        return response.id to response.parentId
    }

    private fun createTestFolder(): Long {
        val request = CreateFolderRequest()
            .apiKey(testApiKey)
            .name("test-folder")
            .pass(FOLDER_PASS)

        return api.createFolderPost(request).id
    }

    private companion object {
        const val API_TOKEN = "55942ee3894f51000530894"
        const val PHONE_NUMBER = "+16463742122"
        const val DEVICE_TOKEN = "871284c348e04a9cacab8aca6b2f3c9a"
        const val FOLDER_PASS = "1234"

        const val SUCCESS_STATUS = "ok"
    }

}