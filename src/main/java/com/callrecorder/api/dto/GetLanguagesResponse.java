package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * GetLanguagesResponse
 */
public class GetLanguagesResponse {

    @SerializedName("status")
    private String status = null;

    @SerializedName("msg")
    private String msg = null;

    @SerializedName("languages")
    private List<Language> languages = null;

    public GetLanguagesResponse status(String status) {
        this.status = status;
        return this;
    }


    /**
     * Get status
     * @return status
     **/
    @ApiModelProperty(example = "ok", required = true, value = "")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public GetLanguagesResponse msg(String msg) {
        this.msg = msg;
        return this;
    }


    /**
     * Get msg
     * @return msg
     **/
    @ApiModelProperty(example = "Success", value = "")
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public GetLanguagesResponse languages(List<Language> languages) {
        this.languages = languages;
        return this;
    }

    public GetLanguagesResponse addLanguagesItem(Language languagesItem) {

        if (this.languages == null) {
            this.languages = new ArrayList<>();
        }

        this.languages.add(languagesItem);
        return this;
    }

    /**
     * Get languages
     * @return languages
     **/
    @ApiModelProperty(value = "")
    public List<Language> getLanguages() {
        return languages;
    }
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GetLanguagesResponse getLanguagesResponse = (GetLanguagesResponse) o;
        return Objects.equals(this.status, getLanguagesResponse.status) &&
                Objects.equals(this.msg, getLanguagesResponse.msg) &&
                Objects.equals(this.languages, getLanguagesResponse.languages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, msg, languages);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GetLanguagesResponse {\n");

        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
        sb.append("    languages: ").append(toIndentedString(languages)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static class Language {

        @SerializedName("code")
        private String code = null;

        @SerializedName("name")
        private String name = null;

        public Language code(String code) {
            this.code = code;
            return this;
        }


        /**
         * Get code
         * @return code
         **/
        @ApiModelProperty(example = "en_US", value = "")
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }

        public Language name(String name) {
            this.name = name;
            return this;
        }


        /**
         * Get name
         * @return name
         **/
        @ApiModelProperty(example = "EnglishUS", value = "")
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Language getLanguagesResponseLanguages = (Language) o;
            return Objects.equals(this.code, getLanguagesResponseLanguages.code) &&
                    Objects.equals(this.name, getLanguagesResponseLanguages.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(code, name);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("class Language {\n");

            sb.append("    code: ").append(toIndentedString(code)).append("\n");
            sb.append("    name: ").append(toIndentedString(name)).append("\n");
            sb.append("}");
            return sb.toString();
        }

        /**
         * Convert the given object to string with each line indented by 4 spaces
         * (except the first line).
         */
        private String toIndentedString(Object o) {
            if (o == null) {
                return "null";
            }
            return o.toString().replace("\n", "\n    ");
        }


    }

}



