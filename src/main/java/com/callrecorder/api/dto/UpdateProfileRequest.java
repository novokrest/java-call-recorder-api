package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateProfileRequest
 */
public class UpdateProfileRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("data")
  private ProfileData data = null;
  
  public UpdateProfileRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateProfileRequest data(ProfileData data) {
    this.data = data;
    return this;
  }

  
  /**
  * Get data
  * @return data
  **/
  @ApiModelProperty(value = "")
  public ProfileData getData() {
    return data;
  }
  public void setData(ProfileData data) {
    this.data = data;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateProfileRequest updateProfileRequest = (UpdateProfileRequest) o;
    return Objects.equals(this.apiKey, updateProfileRequest.apiKey) &&
        Objects.equals(this.data, updateProfileRequest.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, data);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateProfileRequest {\n");
    
    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class ProfileData {

    @SerializedName("f_name")
    private String fName = null;

    @SerializedName("l_name")
    private String lName = null;

    @SerializedName("email")
    private String email = null;

    @SerializedName("is_public")
    private Boolean isPublic = null;

    @SerializedName("language")
    private String language = null;

    public ProfileData fName(String fName) {
      this.fName = fName;
      return this;
    }


    /**
     * Get fName
     * @return fName
     **/
    @ApiModelProperty(example = "first", value = "")
    public String getFName() {
      return fName;
    }
    public void setFName(String fName) {
      this.fName = fName;
    }

    public ProfileData lName(String lName) {
      this.lName = lName;
      return this;
    }


    /**
     * Get lName
     * @return lName
     **/
    @ApiModelProperty(example = "last", value = "")
    public String getLName() {
      return lName;
    }
    public void setLName(String lName) {
      this.lName = lName;
    }

    public ProfileData email(String email) {
      this.email = email;
      return this;
    }


    /**
     * Get email
     * @return email
     **/
    @ApiModelProperty(example = "test@mail.com", value = "")
    public String getEmail() {
      return email;
    }
    public void setEmail(String email) {
      this.email = email;
    }

    public ProfileData isPublic(Boolean isPublic) {
      this.isPublic = isPublic;
      return this;
    }


    /**
     * Get isPublic
     * @return isPublic
     **/
    @ApiModelProperty(example = "true", value = "")
    public Boolean isIsPublic() {
      return isPublic;
    }
    public void setIsPublic(Boolean isPublic) {
      this.isPublic = isPublic;
    }

    public ProfileData language(String language) {
      this.language = language;
      return this;
    }


    /**
     * Get language
     * @return language
     **/
    @ApiModelProperty(example = "en_us", value = "")
    public String getLanguage() {
      return language;
    }
    public void setLanguage(String language) {
      this.language = language;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      ProfileData updateProfileRequestData = (ProfileData) o;
      return Objects.equals(this.fName, updateProfileRequestData.fName) &&
              Objects.equals(this.lName, updateProfileRequestData.lName) &&
              Objects.equals(this.email, updateProfileRequestData.email) &&
              Objects.equals(this.isPublic, updateProfileRequestData.isPublic) &&
              Objects.equals(this.language, updateProfileRequestData.language);
    }

    @Override
    public int hashCode() {
      return Objects.hash(fName, lName, email, isPublic, language);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class ProfileData {\n");

      sb.append("    fName: ").append(toIndentedString(fName)).append("\n");
      sb.append("    lName: ").append(toIndentedString(lName)).append("\n");
      sb.append("    email: ").append(toIndentedString(email)).append("\n");
      sb.append("    isPublic: ").append(toIndentedString(isPublic)).append("\n");
      sb.append("    language: ").append(toIndentedString(language)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }


  }


}



