package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateDeviceTokenRequest
 */
public class UpdateDeviceTokenRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("device_token")
  private String deviceToken = null;
  
  @SerializedName("device_type")
  private DeviceType deviceType = null;
  
  public UpdateDeviceTokenRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateDeviceTokenRequest deviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
    return this;
  }

  
  /**
  * Get deviceToken
  * @return deviceToken
  **/
  @ApiModelProperty(example = "871284c348e04a9cacab8aca6b2f3c9a", value = "")
  public String getDeviceToken() {
    return deviceToken;
  }
  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }
  
  public UpdateDeviceTokenRequest deviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
    return this;
  }

  
  /**
  * Get deviceType
  * @return deviceType
  **/
  @ApiModelProperty(value = "")
  public DeviceType getDeviceType() {
    return deviceType;
  }
  public void setDeviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateDeviceTokenRequest updateDeviceTokenRequest = (UpdateDeviceTokenRequest) o;
    return Objects.equals(this.apiKey, updateDeviceTokenRequest.apiKey) &&
        Objects.equals(this.deviceToken, updateDeviceTokenRequest.deviceToken) &&
        Objects.equals(this.deviceType, updateDeviceTokenRequest.deviceType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, deviceToken, deviceType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateDeviceTokenRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    deviceToken: ").append(toIndentedString(deviceToken)).append("\n");
    sb.append("    deviceType: ").append(toIndentedString(deviceType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



