package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * CreateFolderResponse
 */
public class CreateFolderResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("msg")
  private String msg = null;
  
  @SerializedName("id")
  private Long id = null;
  
  @SerializedName("code")
  private String code = null;
  
  public CreateFolderResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public CreateFolderResponse msg(String msg) {
    this.msg = msg;
    return this;
  }

  
  /**
  * Get msg
  * @return msg
  **/
  @ApiModelProperty(example = "Successfully", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  public CreateFolderResponse id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "464", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public CreateFolderResponse code(String code) {
    this.code = code;
    return this;
  }

  
  /**
  * Get code
  * @return code
  **/
  @ApiModelProperty(example = "folder_created", value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateFolderResponse createFolderResponse = (CreateFolderResponse) o;
    return Objects.equals(this.status, createFolderResponse.status) &&
        Objects.equals(this.msg, createFolderResponse.msg) &&
        Objects.equals(this.id, createFolderResponse.id) &&
        Objects.equals(this.code, createFolderResponse.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, msg, id, code);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateFolderResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



