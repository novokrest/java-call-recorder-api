package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.Objects;

/**
 * GetPhonesResponse
 */
public class GetPhonesResponse extends ArrayList<GetPhonesResponse.PhoneInfo> {

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    return super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetPhonesResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class PhoneInfo {

    @SerializedName("phone_number")
    private String phoneNumber = null;

    @SerializedName("number")
    private String number = null;

    @SerializedName("prefix")
    private String prefix = null;

    @SerializedName("friendly_name")
    private String friendlyName = null;

    @SerializedName("flag")
    private String flag = null;

    @SerializedName("city")
    private String city = null;

    @SerializedName("country")
    private String country = null;

    public PhoneInfo phoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
      return this;
    }


    /**
     * Get phoneNumber
     * @return phoneNumber
     **/
    @ApiModelProperty(example = "+14096008006", value = "")
    public String getPhoneNumber() {
      return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }

    public PhoneInfo number(String number) {
      this.number = number;
      return this;
    }


    /**
     * Get number
     * @return number
     **/
    @ApiModelProperty(example = "4096008006", value = "")
    public String getNumber() {
      return number;
    }
    public void setNumber(String number) {
      this.number = number;
    }

    public PhoneInfo prefix(String prefix) {
      this.prefix = prefix;
      return this;
    }


    /**
     * Get prefix
     * @return prefix
     **/
    @ApiModelProperty(example = "+1", value = "")
    public String getPrefix() {
      return prefix;
    }
    public void setPrefix(String prefix) {
      this.prefix = prefix;
    }

    public PhoneInfo friendlyName(String friendlyName) {
      this.friendlyName = friendlyName;
      return this;
    }


    /**
     * Get friendlyName
     * @return friendlyName
     **/
    @ApiModelProperty(example = "(409) 600-8006", value = "")
    public String getFriendlyName() {
      return friendlyName;
    }
    public void setFriendlyName(String friendlyName) {
      this.friendlyName = friendlyName;
    }

    public PhoneInfo flag(String flag) {
      this.flag = flag;
      return this;
    }


    /**
     * Get flag
     * @return flag
     **/
    @ApiModelProperty(example = "https://app2.virtualbrix.net/assets/flags/icons/us.png", value = "")
    public String getFlag() {
      return flag;
    }
    public void setFlag(String flag) {
      this.flag = flag;
    }

    public PhoneInfo city(String city) {
      this.city = city;
      return this;
    }


    /**
     * Get city
     * @return city
     **/
    @ApiModelProperty(example = "Detroit, MI", value = "")
    public String getCity() {
      return city;
    }
    public void setCity(String city) {
      this.city = city;
    }

    public PhoneInfo country(String country) {
      this.country = country;
      return this;
    }


    /**
     * Get country
     * @return country
     **/
    @ApiModelProperty(example = "United States", value = "")
    public String getCountry() {
      return country;
    }
    public void setCountry(String country) {
      this.country = country;
    }

    @Override
    public boolean equals(java.lang.Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      PhoneInfo getPhonesResponseInner = (PhoneInfo) o;
      return Objects.equals(this.phoneNumber, getPhonesResponseInner.phoneNumber) &&
              Objects.equals(this.number, getPhonesResponseInner.number) &&
              Objects.equals(this.prefix, getPhonesResponseInner.prefix) &&
              Objects.equals(this.friendlyName, getPhonesResponseInner.friendlyName) &&
              Objects.equals(this.flag, getPhonesResponseInner.flag) &&
              Objects.equals(this.city, getPhonesResponseInner.city) &&
              Objects.equals(this.country, getPhonesResponseInner.country);
    }

    @Override
    public int hashCode() {
      return Objects.hash(phoneNumber, number, prefix, friendlyName, flag, city, country);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class PhoneInfo {\n");

      sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
      sb.append("    number: ").append(toIndentedString(number)).append("\n");
      sb.append("    prefix: ").append(toIndentedString(prefix)).append("\n");
      sb.append("    friendlyName: ").append(toIndentedString(friendlyName)).append("\n");
      sb.append("    flag: ").append(toIndentedString(flag)).append("\n");
      sb.append("    city: ").append(toIndentedString(city)).append("\n");
      sb.append("    country: ").append(toIndentedString(country)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }
    
  }
  
}
