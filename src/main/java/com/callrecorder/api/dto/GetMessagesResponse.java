package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * GetMessagesResponse
 */
public class GetMessagesResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("msgs")
  private List<Message> msgs = null;
  
  public GetMessagesResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public GetMessagesResponse msgs(List<Message> msgs) {
    this.msgs = msgs;
    return this;
  }

  public GetMessagesResponse addMsgsItem(Message msgsItem) {
    
    if (this.msgs == null) {
      this.msgs = new ArrayList<>();
    }
    
    this.msgs.add(msgsItem);
    return this;
  }
  
  /**
  * Get msgs
  * @return msgs
  **/
  @ApiModelProperty(value = "")
  public List<Message> getMsgs() {
    return msgs;
  }
  public void setMsgs(List<Message> msgs) {
    this.msgs = msgs;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetMessagesResponse getMessagesResponse = (GetMessagesResponse) o;
    return Objects.equals(this.status, getMessagesResponse.status) &&
        Objects.equals(this.msgs, getMessagesResponse.msgs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, msgs);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetMessagesResponse {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    msgs: ").append(toIndentedString(msgs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class Message {

    @SerializedName("id")
    private Long id = null;

    @SerializedName("title")
    private String title = null;

    @SerializedName("body")
    private String body = null;

    @SerializedName("time")
    private LocalDateTime time = null;

    public Message id(Long id) {
      this.id = id;
      return this;
    }


    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "10", value = "")
    public Long getId() {
      return id;
    }
    public void setId(Long id) {
      this.id = id;
    }

    public Message title(String title) {
      this.title = title;
      return this;
    }


    /**
     * Get title
     * @return title
     **/
    @ApiModelProperty(example = "Title", value = "")
    public String getTitle() {
      return title;
    }
    public void setTitle(String title) {
      this.title = title;
    }

    public Message body(String body) {
      this.body = body;
      return this;
    }


    /**
     * Get body
     * @return body
     **/
    @ApiModelProperty(example = "Message", value = "")
    public String getBody() {
      return body;
    }
    public void setBody(String body) {
      this.body = body;
    }

    public Message time(LocalDateTime time) {
      this.time = time;
      return this;
    }


    /**
     * Get time
     * @return time
     **/
    @ApiModelProperty(value = "")
    public LocalDateTime getTime() {
      return time;
    }
    public void setTime(LocalDateTime time) {
      this.time = time;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Message getMessagesResponseMsgs = (Message) o;
      return Objects.equals(this.id, getMessagesResponseMsgs.id) &&
              Objects.equals(this.title, getMessagesResponseMsgs.title) &&
              Objects.equals(this.body, getMessagesResponseMsgs.body) &&
              Objects.equals(this.time, getMessagesResponseMsgs.time);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id, title, body, time);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class Message {\n");

      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    title: ").append(toIndentedString(title)).append("\n");
      sb.append("    body: ").append(toIndentedString(body)).append("\n");
      sb.append("    time: ").append(toIndentedString(time)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }


  }
  
}



