package com.callrecorder.api.dto;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModelProperty;

import java.io.IOException;
import java.util.Objects;

/**
 * GetFilesRequest
 */
public class GetFilesRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("page")
  private String page = null;
  
  @SerializedName("folder_id")
  private Long folderId = null;
  
  /**
   * Gets or Sets source
   */
  @JsonAdapter(SourceEnum.Adapter.class)
  public enum SourceEnum {
    
    ALL("all"),
    APP2("app2");

    private String value;

    SourceEnum(String value) {
      this.value = value;
    }
    
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    
    public static SourceEnum fromValue(String text) {
      for (SourceEnum b : SourceEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
    
    public static class Adapter extends TypeAdapter<SourceEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final SourceEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public SourceEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return SourceEnum.fromValue(String.valueOf(value));
      }
    }
  }
  
  @SerializedName("source")
  private SourceEnum source = null;
  
  @SerializedName("pass")
  private String pass = null;
  
  @SerializedName("reminder")
  private Boolean reminder = null;
  
  @SerializedName("q")
  private String q = null;
  
  @SerializedName("id")
  private Long id = null;

  @JsonAdapter(OpEnum.Adapter.class)
  public enum OpEnum {

    LESS("less"),
    GREATER("greater");

    private String value;

    OpEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static OpEnum fromValue(String text) {
      for (OpEnum b : OpEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<OpEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final OpEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public OpEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return OpEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("op")
  private OpEnum op = null;
  
  public GetFilesRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "557872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public GetFilesRequest page(String page) {
    this.page = page;
    return this;
  }

  
  /**
  * Get page
  * @return page
  **/
  @ApiModelProperty(example = "0", value = "")
  public String getPage() {
    return page;
  }
  public void setPage(String page) {
    this.page = page;
  }
  
  public GetFilesRequest folderId(Long folderId) {
    this.folderId = folderId;
    return this;
  }

  
  /**
  * Get folderId
  * @return folderId
  **/
  @ApiModelProperty(example = "4", value = "")
  public Long getFolderId() {
    return folderId;
  }
  public void setFolderId(Long folderId) {
    this.folderId = folderId;
  }
  
  public GetFilesRequest source(SourceEnum source) {
    this.source = source;
    return this;
  }

  
  /**
  * Get source
  * @return source
  **/
  @ApiModelProperty(example = "all", value = "")
  public SourceEnum getSource() {
    return source;
  }
  public void setSource(SourceEnum source) {
    this.source = source;
  }
  
  public GetFilesRequest pass(String pass) {
    this.pass = pass;
    return this;
  }

  
  /**
  * Get pass
  * @return pass
  **/
  @ApiModelProperty(example = "1234", value = "")
  public String getPass() {
    return pass;
  }
  public void setPass(String pass) {
    this.pass = pass;
  }
  
  public GetFilesRequest reminder(Boolean reminder) {
    this.reminder = reminder;
    return this;
  }

  
  /**
  * Get reminder
  * @return reminder
  **/
  @ApiModelProperty(value = "")
  public Boolean isReminder() {
    return reminder;
  }
  public void setReminder(Boolean reminder) {
    this.reminder = reminder;
  }
  
  public GetFilesRequest q(String q) {
    this.q = q;
    return this;
  }

  
  /**
  * Get q
  * @return q
  **/
  @ApiModelProperty(example = "hello", value = "")
  public String getQ() {
    return q;
  }
  public void setQ(String q) {
    this.q = q;
  }
  
  public GetFilesRequest id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public GetFilesRequest op(OpEnum op) {
    this.op = op;
    return this;
  }

  
  /**
  * Get op
  * @return op
  **/
  @ApiModelProperty(example = "[\"less\",\"greater\"]", value = "")
  public OpEnum getOp() {
    return op;
  }
  public void setOp(OpEnum op) {
    this.op = op;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilesRequest getFilesRequest = (GetFilesRequest) o;
    return Objects.equals(this.apiKey, getFilesRequest.apiKey) &&
        Objects.equals(this.page, getFilesRequest.page) &&
        Objects.equals(this.folderId, getFilesRequest.folderId) &&
        Objects.equals(this.source, getFilesRequest.source) &&
        Objects.equals(this.pass, getFilesRequest.pass) &&
        Objects.equals(this.reminder, getFilesRequest.reminder) &&
        Objects.equals(this.q, getFilesRequest.q) &&
        Objects.equals(this.id, getFilesRequest.id) &&
        Objects.equals(this.op, getFilesRequest.op);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, page, folderId, source, pass, reminder, q, id, op);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilesRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    folderId: ").append(toIndentedString(folderId)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    pass: ").append(toIndentedString(pass)).append("\n");
    sb.append("    reminder: ").append(toIndentedString(reminder)).append("\n");
    sb.append("    q: ").append(toIndentedString(q)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    op: ").append(toIndentedString(op)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



