package com.callrecorder.api.dto;


import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;


/**
 * Gets or Sets PlayBeep
 */
@JsonAdapter(PlayBeep.Adapter.class)
public enum PlayBeep {
  
  YES("yes"),
  
  NO("no");

  private String value;

  PlayBeep(String value) {
    this.value = value;
  }


  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }


  public static PlayBeep fromValue(String text) {
    for (PlayBeep b : PlayBeep.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }


  public static class Adapter extends TypeAdapter<PlayBeep> {
    @Override
    public void write(final JsonWriter jsonWriter, final PlayBeep enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public PlayBeep read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return PlayBeep.fromValue(String.valueOf(value));
    }
  }

}



