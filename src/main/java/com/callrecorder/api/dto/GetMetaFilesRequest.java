package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * GetMetaFilesRequest
 */
public class GetMetaFilesRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("parent_id")
  private Long parentId = null;
  
  public GetMetaFilesRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public GetMetaFilesRequest parentId(Long parentId) {
    this.parentId = parentId;
    return this;
  }

  
  /**
  * Get parentId
  * @return parentId
  **/
  @ApiModelProperty(example = "577", value = "")
  public Long getParentId() {
    return parentId;
  }
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetMetaFilesRequest getMetaFilesRequest = (GetMetaFilesRequest) o;
    return Objects.equals(this.apiKey, getMetaFilesRequest.apiKey) &&
        Objects.equals(this.parentId, getMetaFilesRequest.parentId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, parentId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetMetaFilesRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    parentId: ").append(toIndentedString(parentId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



