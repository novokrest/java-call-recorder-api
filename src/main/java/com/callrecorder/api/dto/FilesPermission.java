package com.callrecorder.api.dto;


import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;


/**
 * Gets or Sets FilesPermission
 */
@JsonAdapter(FilesPermission.Adapter.class)
public enum FilesPermission {
  
  PUBLIC("public"),
  
  PRIVATE("private");

  private String value;

  FilesPermission(String value) {
    this.value = value;
  }


  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }


  public static FilesPermission fromValue(String text) {
    for (FilesPermission b : FilesPermission.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }


  public static class Adapter extends TypeAdapter<FilesPermission> {
    @Override
    public void write(final JsonWriter jsonWriter, final FilesPermission enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public FilesPermission read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return FilesPermission.fromValue(String.valueOf(value));
    }
  }

}



