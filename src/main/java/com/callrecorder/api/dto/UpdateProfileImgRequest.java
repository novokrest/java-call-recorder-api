package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.io.File;
import java.util.Objects;

/**
 * UpdateProfileImgRequest
 */
public class UpdateProfileImgRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("file")
  private File file = null;
  
  public UpdateProfileImgRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateProfileImgRequest file(File file) {
    this.file = file;
    return this;
  }

  
  /**
  * Get file
  * @return file
  **/
  @ApiModelProperty(value = "")
  public File getFile() {
    return file;
  }
  public void setFile(File file) {
    this.file = file;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateProfileImgRequest updateProfileImgRequest = (UpdateProfileImgRequest) o;
    return Objects.equals(this.apiKey, updateProfileImgRequest.apiKey) &&
        Objects.equals(this.file, updateProfileImgRequest.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, file);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateProfileImgRequest {\n");
    
    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



