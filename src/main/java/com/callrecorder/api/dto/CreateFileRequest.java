package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.io.File;
import java.util.Objects;

/**
 * CreateFileRequest
 */
public class CreateFileRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("file")
  private File file = null;
  
  @SerializedName("data")
  private CreateFileData data = null;
  
  public CreateFileRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "557872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public CreateFileRequest file(File file) {
    this.file = file;
    return this;
  }

  
  /**
  * Get file
  * @return file
  **/
  @ApiModelProperty(required = true, value = "")
  public File getFile() {
    return file;
  }
  public void setFile(File file) {
    this.file = file;
  }
  
  public CreateFileRequest data(CreateFileData data) {
    this.data = data;
    return this;
  }

  
  /**
  * Get data
  * @return data
  **/
  @ApiModelProperty(value = "")
  public CreateFileData getData() {
    return data;
  }
  public void setData(CreateFileData data) {
    this.data = data;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateFileRequest createFileRequest = (CreateFileRequest) o;
    return Objects.equals(this.apiKey, createFileRequest.apiKey) &&
        Objects.equals(this.file, createFileRequest.file) &&
        Objects.equals(this.data, createFileRequest.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, file, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateFileRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



