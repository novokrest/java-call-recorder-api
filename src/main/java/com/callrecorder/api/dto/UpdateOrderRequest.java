package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * UpdateOrderRequest
 */
public class UpdateOrderRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("folders")
  private List<FolderOrder> folders = null;
  
  public UpdateOrderRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateOrderRequest folders(List<FolderOrder> folders) {
    this.folders = folders;
    return this;
  }

  public UpdateOrderRequest addFoldersItem(FolderOrder foldersItem) {
    
    if (this.folders == null) {
      this.folders = new ArrayList<>();
    }
    
    this.folders.add(foldersItem);
    return this;
  }
  
  /**
  * Get folders
  * @return folders
  **/
  @ApiModelProperty(value = "")
  public List<FolderOrder> getFolders() {
    return folders;
  }
  public void setFolders(List<FolderOrder> folders) {
    this.folders = folders;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateOrderRequest updateOrderRequest = (UpdateOrderRequest) o;
    return Objects.equals(this.apiKey, updateOrderRequest.apiKey) &&
        Objects.equals(this.folders, updateOrderRequest.folders);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, folders);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateOrderRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    folders: ").append(toIndentedString(folders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class FolderOrder {

    @SerializedName("id")
    private Long id = null;

    @SerializedName("order_id")
    private Long orderId = null;

    public FolderOrder id(Long id) {
      this.id = id;
      return this;
    }


    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "10", value = "")
    public Long getId() {
      return id;
    }
    public void setId(Long id) {
      this.id = id;
    }

    public FolderOrder orderId(Long orderId) {
      this.orderId = orderId;
      return this;
    }


    /**
     * Get orderId
     * @return orderId
     **/
    @ApiModelProperty(example = "464", value = "")
    public Long getOrderId() {
      return orderId;
    }
    public void setOrderId(Long orderId) {
      this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      FolderOrder updateOrderRequestFolders = (FolderOrder) o;
      return Objects.equals(this.id, updateOrderRequestFolders.id) &&
              Objects.equals(this.orderId, updateOrderRequestFolders.orderId);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id, orderId);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class FolderOrder {\n");

      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }

  }
  
}



