package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * DeleteFilesResponse
 */
public class DeleteFilesResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("msg")
  private String msg = null;
  
  public DeleteFilesResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public DeleteFilesResponse msg(String msg) {
    this.msg = msg;
    return this;
  }

  
  /**
  * Get msg
  * @return msg
  **/
  @ApiModelProperty(example = "Successfully", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeleteFilesResponse deleteFilesResponse = (DeleteFilesResponse) o;
    return Objects.equals(this.status, deleteFilesResponse.status) &&
        Objects.equals(this.msg, deleteFilesResponse.msg);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, msg);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeleteFilesResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



