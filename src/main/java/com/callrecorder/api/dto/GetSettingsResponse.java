package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * GetSettingsResponse
 */
public class GetSettingsResponse {

  @SerializedName("status")
  private String status = null;

  @SerializedName("app")
  private App app = null;
  
  @SerializedName("credits")
  private Long credits = null;
  
  @SerializedName("settings")
  private Settings settings = null;
  
  public GetSettingsResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public GetSettingsResponse app(App app) {
    this.app = app;
    return this;
  }

  
  /**
  * Get app
  * @return app
  **/
  @ApiModelProperty(example = "rec", value = "")
  public App getApp() {
    return app;
  }
  public void setApp(App app) {
    this.app = app;
  }
  
  public GetSettingsResponse credits(Long credits) {
    this.credits = credits;
    return this;
  }

  
  /**
  * Get credits
  * @return credits
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getCredits() {
    return credits;
  }
  public void setCredits(Long credits) {
    this.credits = credits;
  }
  
  public GetSettingsResponse settings(Settings settings) {
    this.settings = settings;
    return this;
  }

  
  /**
  * Get settings
  * @return settings
  **/
  @ApiModelProperty(value = "")
  public Settings getSettings() {
    return settings;
  }
  public void setSettings(Settings settings) {
    this.settings = settings;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetSettingsResponse getSettingsResponse = (GetSettingsResponse) o;
    return Objects.equals(this.status, getSettingsResponse.status) &&
        Objects.equals(this.app, getSettingsResponse.app) &&
        Objects.equals(this.credits, getSettingsResponse.credits) &&
        Objects.equals(this.settings, getSettingsResponse.settings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, app, credits, settings);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetSettingsResponse {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    app: ").append(toIndentedString(app)).append("\n");
    sb.append("    credits: ").append(toIndentedString(credits)).append("\n");
    sb.append("    settings: ").append(toIndentedString(settings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class Settings {

    @SerializedName("play_beep")
    private PlayBeep playBeep = null;

    @SerializedName("files_permission")
    private FilesPermission filesPermission = null;

    public Settings playBeep(PlayBeep playBeep) {
      this.playBeep = playBeep;
      return this;
    }


    /**
     * Get playBeep
     * @return playBeep
     **/
    @ApiModelProperty(value = "")
    public PlayBeep getPlayBeep() {
      return playBeep;
    }
    public void setPlayBeep(PlayBeep playBeep) {
      this.playBeep = playBeep;
    }

    public Settings filesPermission(FilesPermission filesPermission) {
      this.filesPermission = filesPermission;
      return this;
    }


    /**
     * Get filesPermission
     * @return filesPermission
     **/
    @ApiModelProperty(value = "")
    public FilesPermission getFilesPermission() {
      return filesPermission;
    }
    public void setFilesPermission(FilesPermission filesPermission) {
      this.filesPermission = filesPermission;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Settings getSettingsResponseSettings = (Settings) o;
      return Objects.equals(this.playBeep, getSettingsResponseSettings.playBeep) &&
              Objects.equals(this.filesPermission, getSettingsResponseSettings.filesPermission);
    }

    @Override
    public int hashCode() {
      return Objects.hash(playBeep, filesPermission);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class Settings {\n");

      sb.append("    playBeep: ").append(toIndentedString(playBeep)).append("\n");
      sb.append("    filesPermission: ").append(toIndentedString(filesPermission)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }


  }

}



