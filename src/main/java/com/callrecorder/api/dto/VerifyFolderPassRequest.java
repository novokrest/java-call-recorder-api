package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * VerifyFolderPassRequest
 */
public class VerifyFolderPassRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("id")
  private Long id = null;
  
  @SerializedName("pass")
  private String pass = null;
  
  public VerifyFolderPassRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public VerifyFolderPassRequest id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "464", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public VerifyFolderPassRequest pass(String pass) {
    this.pass = pass;
    return this;
  }

  
  /**
  * Get pass
  * @return pass
  **/
  @ApiModelProperty(example = "testpass", value = "")
  public String getPass() {
    return pass;
  }
  public void setPass(String pass) {
    this.pass = pass;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VerifyFolderPassRequest verifyFolderPassRequest = (VerifyFolderPassRequest) o;
    return Objects.equals(this.apiKey, verifyFolderPassRequest.apiKey) &&
        Objects.equals(this.id, verifyFolderPassRequest.id) &&
        Objects.equals(this.pass, verifyFolderPassRequest.pass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, id, pass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VerifyFolderPassRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    pass: ").append(toIndentedString(pass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



