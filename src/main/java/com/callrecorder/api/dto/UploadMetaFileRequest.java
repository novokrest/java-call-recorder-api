package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.io.File;
import java.util.Objects;

/**
 * UploadMetaFileRequest
 */
public class UploadMetaFileRequest {

  @SerializedName("api_key")
  private String apiKey = null;

  @SerializedName("file")
  private File file = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("parent_id")
  private Long parentId = null;

  @SerializedName("id")
  private Long id = null;

  public UploadMetaFileRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }


  /**
   * Get apiKey
   * @return apiKey
   **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public UploadMetaFileRequest file(File file) {
    this.file = file;
    return this;
  }


  /**
   * Get file
   * @return file
   **/
  @ApiModelProperty(value = "")
  public File getFile() {
    return file;
  }
  public void setFile(File file) {
    this.file = file;
  }

  public UploadMetaFileRequest name(String name) {
    this.name = name;
    return this;
  }


  /**
   * Get name
   * @return name
   **/
  @ApiModelProperty(example = "test-name", value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public UploadMetaFileRequest parentId(Long parentId) {
    this.parentId = parentId;
    return this;
  }


  /**
   * Get parentId
   * @return parentId
   **/
  @ApiModelProperty(example = "577", value = "")
  public Long getParentId() {
    return parentId;
  }
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public UploadMetaFileRequest id(Long id) {
    this.id = id;
    return this;
  }


  /**
   * Get id
   * @return id
   **/
  @ApiModelProperty(example = "645", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UploadMetaFileRequest uploadMetaFileRequest = (UploadMetaFileRequest) o;
    return Objects.equals(this.apiKey, uploadMetaFileRequest.apiKey) &&
            Objects.equals(this.file, uploadMetaFileRequest.file) &&
            Objects.equals(this.name, uploadMetaFileRequest.name) &&
            Objects.equals(this.parentId, uploadMetaFileRequest.parentId) &&
            Objects.equals(this.id, uploadMetaFileRequest.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, file, name, parentId, id);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UploadMetaFileRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    parentId: ").append(toIndentedString(parentId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}
