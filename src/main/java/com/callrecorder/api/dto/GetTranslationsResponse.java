package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;
import java.util.Objects;

/**
 * GetTranslationsResponse
 */
public class GetTranslationsResponse {

  @SerializedName("status")
  private String status = null;

  @SerializedName("msg")
  private String msg = null;

  @SerializedName("code")
  private String code = null;

  @SerializedName("translation")
  private Map<String, String> translation = null;

  public GetTranslationsResponse status(String status) {
    this.status = status;
    return this;
  }


  /**
   * Get status
   * @return status
   **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  public GetTranslationsResponse msg(String msg) {
    this.msg = msg;
    return this;
  }


  /**
   * Get msg
   * @return msg
   **/
  @ApiModelProperty(example = "Success", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }

  public GetTranslationsResponse code(String code) {
    this.code = code;
    return this;
  }


  /**
   * Get code
   * @return code
   **/
  @ApiModelProperty(example = "no_language", value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  public GetTranslationsResponse translation(Map<String, String> translation) {
    this.translation = translation;
    return this;
  }

  public GetTranslationsResponse putTranslationItem(String key, String translationItem) {

    if (this.translation == null) {
      this.translation = null;
    }

    this.translation.put(key, translationItem);
    return this;
  }
  /**
   * Get translation
   * @return translation
   **/
  @ApiModelProperty(value = "")
  public Map<String, String> getTranslation() {
    return translation;
  }
  public void setTranslation(Map<String, String> translation) {
    this.translation = translation;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetTranslationsResponse getTranslationsResponse = (GetTranslationsResponse) o;
    return Objects.equals(this.status, getTranslationsResponse.status) &&
            Objects.equals(this.msg, getTranslationsResponse.msg) &&
            Objects.equals(this.code, getTranslationsResponse.code) &&
            Objects.equals(this.translation, getTranslationsResponse.translation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, msg, code, translation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetTranslationsResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    translation: ").append(toIndentedString(translation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}
