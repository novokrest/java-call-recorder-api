package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * GetProfileResponse
 */
public class GetProfileResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("code")
  private String code = null;
  
  @SerializedName("profile")
  private ProfileInfo profile = null;
  
  @SerializedName("app")
  private App app = null;
  
  @SerializedName("share_url")
  private String shareUrl = null;
  
  @SerializedName("rate_url")
  private String rateUrl = null;
  
  @SerializedName("credits")
  private Long credits = null;
  
  @SerializedName("credits_trans")
  private Long creditsTrans = null;
  
  public GetProfileResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public GetProfileResponse code(String code) {
    this.code = code;
    return this;
  }

  
  /**
  * Get code
  * @return code
  **/
  @ApiModelProperty(example = "user_profile", value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }
  
  public GetProfileResponse profile(ProfileInfo profile) {
    this.profile = profile;
    return this;
  }

  
  /**
  * Get profile
  * @return profile
  **/
  @ApiModelProperty(value = "")
  public ProfileInfo getProfile() {
    return profile;
  }
  public void setProfile(ProfileInfo profile) {
    this.profile = profile;
  }
  
  public GetProfileResponse app(App app) {
    this.app = app;
    return this;
  }

  
  /**
  * Get app
  * @return app
  **/
  @ApiModelProperty(example = "rec", value = "")
  public App getApp() {
    return app;
  }
  public void setApp(App app) {
    this.app = app;
  }
  
  public GetProfileResponse shareUrl(String shareUrl) {
    this.shareUrl = shareUrl;
    return this;
  }

  
  /**
  * Get shareUrl
  * @return shareUrl
  **/
  @ApiModelProperty(example = "itms-apps://itunes.apple.com/app/962899849", value = "")
  public String getShareUrl() {
    return shareUrl;
  }
  public void setShareUrl(String shareUrl) {
    this.shareUrl = shareUrl;
  }
  
  public GetProfileResponse rateUrl(String rateUrl) {
    this.rateUrl = rateUrl;
    return this;
  }

  
  /**
  * Get rateUrl
  * @return rateUrl
  **/
  @ApiModelProperty(example = "itms-apps://itunes.apple.com/app/962899849", value = "")
  public String getRateUrl() {
    return rateUrl;
  }
  public void setRateUrl(String rateUrl) {
    this.rateUrl = rateUrl;
  }
  
  public GetProfileResponse credits(Long credits) {
    this.credits = credits;
    return this;
  }

  
  /**
  * Get credits
  * @return credits
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getCredits() {
    return credits;
  }
  public void setCredits(Long credits) {
    this.credits = credits;
  }
  
  public GetProfileResponse creditsTrans(Long creditsTrans) {
    this.creditsTrans = creditsTrans;
    return this;
  }

  
  /**
  * Get creditsTrans
  * @return creditsTrans
  **/
  @ApiModelProperty(example = "0", value = "")
  public Long getCreditsTrans() {
    return creditsTrans;
  }
  public void setCreditsTrans(Long creditsTrans) {
    this.creditsTrans = creditsTrans;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetProfileResponse getProfileResponse = (GetProfileResponse) o;
    return Objects.equals(this.status, getProfileResponse.status) &&
        Objects.equals(this.code, getProfileResponse.code) &&
        Objects.equals(this.profile, getProfileResponse.profile) &&
        Objects.equals(this.app, getProfileResponse.app) &&
        Objects.equals(this.shareUrl, getProfileResponse.shareUrl) &&
        Objects.equals(this.rateUrl, getProfileResponse.rateUrl) &&
        Objects.equals(this.credits, getProfileResponse.credits) &&
        Objects.equals(this.creditsTrans, getProfileResponse.creditsTrans);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, code, profile, app, shareUrl, rateUrl, credits, creditsTrans);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetProfileResponse {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    profile: ").append(toIndentedString(profile)).append("\n");
    sb.append("    app: ").append(toIndentedString(app)).append("\n");
    sb.append("    shareUrl: ").append(toIndentedString(shareUrl)).append("\n");
    sb.append("    rateUrl: ").append(toIndentedString(rateUrl)).append("\n");
    sb.append("    credits: ").append(toIndentedString(credits)).append("\n");
    sb.append("    creditsTrans: ").append(toIndentedString(creditsTrans)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class ProfileInfo {

    @SerializedName("f_name")
    private String fName = null;

    @SerializedName("l_name")
    private String lName = null;

    @SerializedName("email")
    private String email = null;

    @SerializedName("phone")
    private String phone = null;

    @SerializedName("pic")
    private String pic = null;

    @SerializedName("language")
    private String language = null;

    @SerializedName("is_public")
    private Integer isPublic = null;

    @SerializedName("play_beep")
    private Integer playBeep = null;

    @SerializedName("max_length")
    private Long maxLength = null;

    @SerializedName("time_zone")
    private String timeZone = null;

    @SerializedName("time")
    private Long time = null;

    @SerializedName("pin")
    private String pin = null;

    public ProfileInfo fName(String fName) {
      this.fName = fName;
      return this;
    }


    /**
     * Get fName
     * @return fName
     **/
    @ApiModelProperty(example = "first", value = "")
    public String getFName() {
      return fName;
    }
    public void setFName(String fName) {
      this.fName = fName;
    }

    public ProfileInfo lName(String lName) {
      this.lName = lName;
      return this;
    }


    /**
     * Get lName
     * @return lName
     **/
    @ApiModelProperty(example = "last", value = "")
    public String getLName() {
      return lName;
    }
    public void setLName(String lName) {
      this.lName = lName;
    }

    public ProfileInfo email(String email) {
      this.email = email;
      return this;
    }


    /**
     * Get email
     * @return email
     **/
    @ApiModelProperty(example = "test@mail.com", value = "")
    public String getEmail() {
      return email;
    }
    public void setEmail(String email) {
      this.email = email;
    }

    public ProfileInfo phone(String phone) {
      this.phone = phone;
      return this;
    }


    /**
     * Get phone
     * @return phone
     **/
    @ApiModelProperty(example = "+16463742122", value = "")
    public String getPhone() {
      return phone;
    }
    public void setPhone(String phone) {
      this.phone = phone;
    }

    public ProfileInfo pic(String pic) {
      this.pic = pic;
      return this;
    }


    /**
     * Get pic
     * @return pic
     **/
    @ApiModelProperty(example = "https://s3.us-east-2.amazonaws.com/vxphone/32/1.png", value = "")
    public String getPic() {
      return pic;
    }
    public void setPic(String pic) {
      this.pic = pic;
    }

    public ProfileInfo language(String language) {
      this.language = language;
      return this;
    }


    /**
     * Get language
     * @return language
     **/
    @ApiModelProperty(example = "en_us", value = "")
    public String getLanguage() {
      return language;
    }
    public void setLanguage(String language) {
      this.language = language;
    }

    public ProfileInfo isPublic(Integer isPublic) {
      this.isPublic = isPublic;
      return this;
    }


    /**
     * Get isPublic
     * @return isPublic
     **/
    @ApiModelProperty(example = "1", value = "")
    public Integer getIsPublic() {
      return isPublic;
    }
    public void setIsPublic(Integer isPublic) {
      this.isPublic = isPublic;
    }

    public ProfileInfo playBeep(Integer playBeep) {
      this.playBeep = playBeep;
      return this;
    }


    /**
     * Get playBeep
     * @return playBeep
     **/
    @ApiModelProperty(example = "0", value = "")
    public Integer getPlayBeep() {
      return playBeep;
    }
    public void setPlayBeep(Integer playBeep) {
      this.playBeep = playBeep;
    }

    public ProfileInfo maxLength(Long maxLength) {
      this.maxLength = maxLength;
      return this;
    }


    /**
     * Get maxLength
     * @return maxLength
     **/
    @ApiModelProperty(example = "0", value = "")
    public Long getMaxLength() {
      return maxLength;
    }
    public void setMaxLength(Long maxLength) {
      this.maxLength = maxLength;
    }

    public ProfileInfo timeZone(String timeZone) {
      this.timeZone = timeZone;
      return this;
    }


    /**
     * Get timeZone
     * @return timeZone
     **/
    @ApiModelProperty(example = "10", value = "")
    public String getTimeZone() {
      return timeZone;
    }
    public void setTimeZone(String timeZone) {
      this.timeZone = timeZone;
    }

    public ProfileInfo time(Long time) {
      this.time = time;
      return this;
    }


    /**
     * Get time
     * @return time
     **/
    @ApiModelProperty(example = "10", value = "")
    public Long getTime() {
      return time;
    }
    public void setTime(Long time) {
      this.time = time;
    }

    public ProfileInfo pin(String pin) {
      this.pin = pin;
      return this;
    }


    /**
     * Get pin
     * @return pin
     **/
    @ApiModelProperty(example = "10", value = "")
    public String getPin() {
      return pin;
    }
    public void setPin(String pin) {
      this.pin = pin;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      ProfileInfo getProfileResponseProfile = (ProfileInfo) o;
      return Objects.equals(this.fName, getProfileResponseProfile.fName) &&
              Objects.equals(this.lName, getProfileResponseProfile.lName) &&
              Objects.equals(this.email, getProfileResponseProfile.email) &&
              Objects.equals(this.phone, getProfileResponseProfile.phone) &&
              Objects.equals(this.pic, getProfileResponseProfile.pic) &&
              Objects.equals(this.language, getProfileResponseProfile.language) &&
              Objects.equals(this.isPublic, getProfileResponseProfile.isPublic) &&
              Objects.equals(this.playBeep, getProfileResponseProfile.playBeep) &&
              Objects.equals(this.maxLength, getProfileResponseProfile.maxLength) &&
              Objects.equals(this.timeZone, getProfileResponseProfile.timeZone) &&
              Objects.equals(this.time, getProfileResponseProfile.time) &&
              Objects.equals(this.pin, getProfileResponseProfile.pin);
    }

    @Override
    public int hashCode() {
      return Objects.hash(fName, lName, email, phone, pic, language, isPublic, playBeep, maxLength, timeZone, time, pin);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class ProfileInfo {\n");

      sb.append("    fName: ").append(toIndentedString(fName)).append("\n");
      sb.append("    lName: ").append(toIndentedString(lName)).append("\n");
      sb.append("    email: ").append(toIndentedString(email)).append("\n");
      sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
      sb.append("    pic: ").append(toIndentedString(pic)).append("\n");
      sb.append("    language: ").append(toIndentedString(language)).append("\n");
      sb.append("    isPublic: ").append(toIndentedString(isPublic)).append("\n");
      sb.append("    playBeep: ").append(toIndentedString(playBeep)).append("\n");
      sb.append("    maxLength: ").append(toIndentedString(maxLength)).append("\n");
      sb.append("    timeZone: ").append(toIndentedString(timeZone)).append("\n");
      sb.append("    time: ").append(toIndentedString(time)).append("\n");
      sb.append("    pin: ").append(toIndentedString(pin)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }


  }

}



