package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RegisterPhoneRequest
 */
public class RegisterPhoneRequest {

  @SerializedName("token")
  private String token = null;
  
  @SerializedName("phone")
  private String phone = null;
  
  public RegisterPhoneRequest token(String token) {
    this.token = token;
    return this;
  }

  
  /**
  * Get token
  * @return token
  **/
  @ApiModelProperty(example = "55942ee3894f5100053089", required = true, value = "")
  public String getToken() {
    return token;
  }
  public void setToken(String token) {
    this.token = token;
  }
  
  public RegisterPhoneRequest phone(String phone) {
    this.phone = phone;
    return this;
  }

  
  /**
  * Get phone
  * @return phone
  **/
  @ApiModelProperty(example = "+16463742122", required = true, value = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterPhoneRequest registerPhoneRequest = (RegisterPhoneRequest) o;
    return Objects.equals(this.token, registerPhoneRequest.token) &&
        Objects.equals(this.phone, registerPhoneRequest.phone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(token, phone);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegisterPhoneRequest {\n");

    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



