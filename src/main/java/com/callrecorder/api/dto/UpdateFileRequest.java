package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * UpdateFileRequest
 */
public class UpdateFileRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("id")
  private Long id = null;
  
  @SerializedName("f_name")
  private String fName = null;
  
  @SerializedName("l_name")
  private String lName = null;
  
  @SerializedName("notes")
  private String notes = null;
  
  @SerializedName("email")
  private String email = null;
  
  @SerializedName("phone")
  private String phone = null;
  
  @SerializedName("tags")
  private String tags = null;
  
  @SerializedName("folder_id")
  private Long folderId = null;
  
  @SerializedName("name")
  private String name = null;
  
  @SerializedName("remind_days")
  private String remindDays = null;
  
  @SerializedName("remind_date")
  private OffsetDateTime remindDate = null;
  
  public UpdateFileRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "557872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateFileRequest id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "11", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public UpdateFileRequest fName(String fName) {
    this.fName = fName;
    return this;
  }

  
  /**
  * Get fName
  * @return fName
  **/
  @ApiModelProperty(example = "firsttest", value = "")
  public String getFName() {
    return fName;
  }
  public void setFName(String fName) {
    this.fName = fName;
  }
  
  public UpdateFileRequest lName(String lName) {
    this.lName = lName;
    return this;
  }

  
  /**
  * Get lName
  * @return lName
  **/
  @ApiModelProperty(example = "lasttest", value = "")
  public String getLName() {
    return lName;
  }
  public void setLName(String lName) {
    this.lName = lName;
  }
  
  public UpdateFileRequest notes(String notes) {
    this.notes = notes;
    return this;
  }

  
  /**
  * Get notes
  * @return notes
  **/
  @ApiModelProperty(example = "test_notes", value = "")
  public String getNotes() {
    return notes;
  }
  public void setNotes(String notes) {
    this.notes = notes;
  }
  
  public UpdateFileRequest email(String email) {
    this.email = email;
    return this;
  }

  
  /**
  * Get email
  * @return email
  **/
  @ApiModelProperty(example = "test@email.com", value = "")
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  
  public UpdateFileRequest phone(String phone) {
    this.phone = phone;
    return this;
  }

  
  /**
  * Get phone
  * @return phone
  **/
  @ApiModelProperty(example = "+16463742122", value = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public UpdateFileRequest tags(String tags) {
    this.tags = tags;
    return this;
  }

  
  /**
  * Get tags
  * @return tags
  **/
  @ApiModelProperty(example = "tagone", value = "")
  public String getTags() {
    return tags;
  }
  public void setTags(String tags) {
    this.tags = tags;
  }
  
  public UpdateFileRequest folderId(Long folderId) {
    this.folderId = folderId;
    return this;
  }

  
  /**
  * Get folderId
  * @return folderId
  **/
  @ApiModelProperty(example = "0", value = "")
  public Long getFolderId() {
    return folderId;
  }
  public void setFolderId(Long folderId) {
    this.folderId = folderId;
  }
  
  public UpdateFileRequest name(String name) {
    this.name = name;
    return this;
  }

  
  /**
  * Get name
  * @return name
  **/
  @ApiModelProperty(example = "first", value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public UpdateFileRequest remindDays(String remindDays) {
    this.remindDays = remindDays;
    return this;
  }

  
  /**
  * Get remindDays
  * @return remindDays
  **/
  @ApiModelProperty(example = "10", value = "")
  public String getRemindDays() {
    return remindDays;
  }
  public void setRemindDays(String remindDays) {
    this.remindDays = remindDays;
  }
  
  public UpdateFileRequest remindDate(OffsetDateTime remindDate) {
    this.remindDate = remindDate;
    return this;
  }

  
  /**
  * Get remindDate
  * @return remindDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getRemindDate() {
    return remindDate;
  }
  public void setRemindDate(OffsetDateTime remindDate) {
    this.remindDate = remindDate;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateFileRequest updateFileRequest = (UpdateFileRequest) o;
    return Objects.equals(this.apiKey, updateFileRequest.apiKey) &&
        Objects.equals(this.id, updateFileRequest.id) &&
        Objects.equals(this.fName, updateFileRequest.fName) &&
        Objects.equals(this.lName, updateFileRequest.lName) &&
        Objects.equals(this.notes, updateFileRequest.notes) &&
        Objects.equals(this.email, updateFileRequest.email) &&
        Objects.equals(this.phone, updateFileRequest.phone) &&
        Objects.equals(this.tags, updateFileRequest.tags) &&
        Objects.equals(this.folderId, updateFileRequest.folderId) &&
        Objects.equals(this.name, updateFileRequest.name) &&
        Objects.equals(this.remindDays, updateFileRequest.remindDays) &&
        Objects.equals(this.remindDate, updateFileRequest.remindDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, id, fName, lName, notes, email, phone, tags, folderId, name, remindDays, remindDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateFileRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    fName: ").append(toIndentedString(fName)).append("\n");
    sb.append("    lName: ").append(toIndentedString(lName)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    folderId: ").append(toIndentedString(folderId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    remindDays: ").append(toIndentedString(remindDays)).append("\n");
    sb.append("    remindDate: ").append(toIndentedString(remindDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



