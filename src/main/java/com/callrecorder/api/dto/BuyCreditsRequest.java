package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * BuyCreditsRequest
 */
public class BuyCreditsRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("amount")
  private Long amount = null;
  
  @SerializedName("receipt")
  private String receipt = null;
  
  @SerializedName("product_id")
  private Long productId = null;
  
  @SerializedName("device_type")
  private DeviceType deviceType = null;
  
  public BuyCreditsRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public BuyCreditsRequest amount(Long amount) {
    this.amount = amount;
    return this;
  }

  
  /**
  * Get amount
  * @return amount
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getAmount() {
    return amount;
  }
  public void setAmount(Long amount) {
    this.amount = amount;
  }
  
  public BuyCreditsRequest receipt(String receipt) {
    this.receipt = receipt;
    return this;
  }

  
  /**
  * Get receipt
  * @return receipt
  **/
  @ApiModelProperty(example = "123", value = "")
  public String getReceipt() {
    return receipt;
  }
  public void setReceipt(String receipt) {
    this.receipt = receipt;
  }
  
  public BuyCreditsRequest productId(Long productId) {
    this.productId = productId;
    return this;
  }

  
  /**
  * Get productId
  * @return productId
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getProductId() {
    return productId;
  }
  public void setProductId(Long productId) {
    this.productId = productId;
  }
  
  public BuyCreditsRequest deviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
    return this;
  }

  
  /**
  * Get deviceType
  * @return deviceType
  **/
  @ApiModelProperty(value = "")
  public DeviceType getDeviceType() {
    return deviceType;
  }
  public void setDeviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BuyCreditsRequest buyCreditsRequest = (BuyCreditsRequest) o;
    return Objects.equals(this.apiKey, buyCreditsRequest.apiKey) &&
        Objects.equals(this.amount, buyCreditsRequest.amount) &&
        Objects.equals(this.receipt, buyCreditsRequest.receipt) &&
        Objects.equals(this.productId, buyCreditsRequest.productId) &&
        Objects.equals(this.deviceType, buyCreditsRequest.deviceType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, amount, receipt, productId, deviceType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BuyCreditsRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    receipt: ").append(toIndentedString(receipt)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    deviceType: ").append(toIndentedString(deviceType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



