package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * DeleteMetaFilesRequest
 */
public class DeleteMetaFilesRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("ids")
  private List<Long> ids = null;
  
  @SerializedName("parent_id")
  private Long parentId = null;
  
  public DeleteMetaFilesRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public DeleteMetaFilesRequest ids(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public DeleteMetaFilesRequest addIdsItem(Long idsItem) {
    
    if (this.ids == null) {
      this.ids = new ArrayList<>();
    }
    
    this.ids.add(idsItem);
    return this;
  }
  
  /**
  * Get ids
  * @return ids
  **/
  @ApiModelProperty(value = "")
  public List<Long> getIds() {
    return ids;
  }
  public void setIds(List<Long> ids) {
    this.ids = ids;
  }
  
  public DeleteMetaFilesRequest parentId(Long parentId) {
    this.parentId = parentId;
    return this;
  }

  
  /**
  * Get parentId
  * @return parentId
  **/
  @ApiModelProperty(example = "577", value = "")
  public Long getParentId() {
    return parentId;
  }
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeleteMetaFilesRequest deleteMetaFilesRequest = (DeleteMetaFilesRequest) o;
    return Objects.equals(this.apiKey, deleteMetaFilesRequest.apiKey) &&
        Objects.equals(this.ids, deleteMetaFilesRequest.ids) &&
        Objects.equals(this.parentId, deleteMetaFilesRequest.parentId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, ids, parentId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeleteMetaFilesRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    ids: ").append(toIndentedString(ids)).append("\n");
    sb.append("    parentId: ").append(toIndentedString(parentId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



