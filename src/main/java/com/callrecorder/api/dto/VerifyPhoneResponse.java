package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * VerifyPhoneResponse
 */
public class VerifyPhoneResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("phone")
  private String phone = null;
  
  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("msg")
  private String msg = null;
  
  public VerifyPhoneResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public VerifyPhoneResponse phone(String phone) {
    this.phone = phone;
    return this;
  }

  
  /**
  * Get phone
  * @return phone
  **/
  @ApiModelProperty(example = "+16463742122", value = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public VerifyPhoneResponse apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "557872b508520557872b50855c", value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public VerifyPhoneResponse msg(String msg) {
    this.msg = msg;
    return this;
  }

  
  /**
  * Get msg
  * @return msg
  **/
  @ApiModelProperty(example = "Phone Verified", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VerifyPhoneResponse verifyPhoneResponse = (VerifyPhoneResponse) o;
    return Objects.equals(this.status, verifyPhoneResponse.status) &&
        Objects.equals(this.phone, verifyPhoneResponse.phone) &&
        Objects.equals(this.apiKey, verifyPhoneResponse.apiKey) &&
        Objects.equals(this.msg, verifyPhoneResponse.msg);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, phone, apiKey, msg);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VerifyPhoneResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



