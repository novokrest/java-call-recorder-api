package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * CreateFolderRequest
 */
public class CreateFolderRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("name")
  private String name = null;
  
  @SerializedName("pass")
  private String pass = null;
  
  public CreateFolderRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public CreateFolderRequest name(String name) {
    this.name = name;
    return this;
  }

  
  /**
  * Get name
  * @return name
  **/
  @ApiModelProperty(example = "testname", value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public CreateFolderRequest pass(String pass) {
    this.pass = pass;
    return this;
  }

  
  /**
  * Get pass
  * @return pass
  **/
  @ApiModelProperty(example = "1234", value = "")
  public String getPass() {
    return pass;
  }
  public void setPass(String pass) {
    this.pass = pass;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateFolderRequest createFolderRequest = (CreateFolderRequest) o;
    return Objects.equals(this.apiKey, createFolderRequest.apiKey) &&
        Objects.equals(this.name, createFolderRequest.name) &&
        Objects.equals(this.pass, createFolderRequest.pass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, name, pass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateFolderRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    pass: ").append(toIndentedString(pass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



