package com.callrecorder.api.dto;


import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;


/**
 * Gets or Sets DeviceType
 */
@JsonAdapter(DeviceType.Adapter.class)
public enum DeviceType {
  
  ANDROID("android"),
  
  IOS("ios"),

  MAC("mac"),
  
  WINDOWS("windows"),
  
  WEB("web"),
  
  CUSTOM("custom");

  private String value;

  DeviceType(String value) {
    this.value = value;
  }


  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }


  public static DeviceType fromValue(String text) {
    for (DeviceType b : DeviceType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }


  public static class Adapter extends TypeAdapter<DeviceType> {
    @Override
    public void write(final JsonWriter jsonWriter, final DeviceType enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public DeviceType read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return DeviceType.fromValue(String.valueOf(value));
    }
  }

}



