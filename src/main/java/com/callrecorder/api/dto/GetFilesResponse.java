package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * GetFilesResponse
 */
public class GetFilesResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("credits")
  private Long credits = null;

  @SerializedName("credits_trans")
  private Long creditsTrans = null;
  
  @SerializedName("files")
  private List<FileInfo> files = null;
  
  public GetFilesResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public GetFilesResponse credits(Long credits) {
    this.credits = credits;
    return this;
  }

  
  /**
  * Get credits
  * @return credits
  **/
  @ApiModelProperty(value = "")
  public Long getCredits() {
    return credits;
  }
  public void setCredits(Long credits) {
    this.credits = credits;
  }

  public GetFilesResponse creditsTrans(Long creditsTrans) {
    this.creditsTrans = creditsTrans;
    return this;
  }


  /**
   * Get creditsTrans
   * @return creditsTrans
   **/
  @ApiModelProperty(value = "")
  public Long getCreditsTrans() {
    return creditsTrans;
  }
  public void setCreditsTrans(Long creditsTrans) {
    this.creditsTrans = creditsTrans;
  }

  public GetFilesResponse files(List<FileInfo> files) {
    this.files = files;
    return this;
  }

  public GetFilesResponse addFilesItem(FileInfo filesItem) {
    
    if (this.files == null) {
      this.files = new ArrayList<>();
    }
    
    this.files.add(filesItem);
    return this;
  }
  
  /**
  * Get files
  * @return files
  **/
  @ApiModelProperty(value = "")
  public List<FileInfo> getFiles() {
    return files;
  }
  public void setFiles(List<FileInfo> files) {
    this.files = files;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFilesResponse getFilesResponse = (GetFilesResponse) o;
    return Objects.equals(this.status, getFilesResponse.status) &&
        Objects.equals(this.credits, getFilesResponse.credits) &&
        Objects.equals(this.creditsTrans, getFilesResponse.creditsTrans) &&
        Objects.equals(this.files, getFilesResponse.files);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, credits, creditsTrans, files);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFilesResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    credits: ").append(toIndentedString(credits)).append("\n");
    sb.append("    creditsTrans: ").append(toIndentedString(creditsTrans)).append("\n");
    sb.append("    files: ").append(toIndentedString(files)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class FileInfo {

    @SerializedName("id")
    private Long id = null;

    @SerializedName("access_number")
    private String accessNumber = null;

    @SerializedName("name")
    private String name = null;

    @SerializedName("f_name")
    private String fName = null;

    @SerializedName("l_name")
    private String lName = null;

    @SerializedName("email")
    private String email = null;

    @SerializedName("phone")
    private String phone = null;

    @SerializedName("notes")
    private String notes = null;

    @SerializedName("meta")
    private String meta = null;

    @SerializedName("source")
    private String source = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("credits")
    private String credits = null;

    @SerializedName("duration")
    private String duration = null;

    @SerializedName("time")
    private String time = null;

    @SerializedName("share_url")
    private String shareUrl = null;

    @SerializedName("download_url")
    private String downloadUrl = null;

    public FileInfo id(Long id) {
      this.id = id;
      return this;
    }


    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "12", value = "")
    public Long getId() {
      return id;
    }
    public void setId(Long id) {
      this.id = id;
    }

    public FileInfo accessNumber(String accessNumber) {
      this.accessNumber = accessNumber;
      return this;
    }


    /**
     * Get accessNumber
     * @return accessNumber
     **/
    @ApiModelProperty(example = "1", value = "")
    public String getAccessNumber() {
      return accessNumber;
    }
    public void setAccessNumber(String accessNumber) {
      this.accessNumber = accessNumber;
    }

    public FileInfo name(String name) {
      this.name = name;
      return this;
    }


    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "Untitled4", value = "")
    public String getName() {
      return name;
    }
    public void setName(String name) {
      this.name = name;
    }

    public FileInfo fName(String fName) {
      this.fName = fName;
      return this;
    }


    /**
     * Get fName
     * @return fName
     **/
    @ApiModelProperty(example = "first", value = "")
    public String getFName() {
      return fName;
    }
    public void setFName(String fName) {
      this.fName = fName;
    }

    public FileInfo lName(String lName) {
      this.lName = lName;
      return this;
    }


    /**
     * Get lName
     * @return lName
     **/
    @ApiModelProperty(example = "last", value = "")
    public String getLName() {
      return lName;
    }
    public void setLName(String lName) {
      this.lName = lName;
    }

    public FileInfo email(String email) {
      this.email = email;
      return this;
    }


    /**
     * Get email
     * @return email
     **/
    @ApiModelProperty(example = "test@mail.com", value = "")
    public String getEmail() {
      return email;
    }
    public void setEmail(String email) {
      this.email = email;
    }

    public FileInfo phone(String phone) {
      this.phone = phone;
      return this;
    }


    /**
     * Get phone
     * @return phone
     **/
    @ApiModelProperty(value = "")
    public String getPhone() {
      return phone;
    }
    public void setPhone(String phone) {
      this.phone = phone;
    }

    public FileInfo notes(String notes) {
      this.notes = notes;
      return this;
    }


    /**
     * Get notes
     * @return notes
     **/
    @ApiModelProperty(value = "")
    public String getNotes() {
      return notes;
    }
    public void setNotes(String notes) {
      this.notes = notes;
    }

    public FileInfo meta(String meta) {
      this.meta = meta;
      return this;
    }


    /**
     * Get meta
     * @return meta
     **/
    @ApiModelProperty(value = "")
    public String getMeta() {
      return meta;
    }
    public void setMeta(String meta) {
      this.meta = meta;
    }

    public FileInfo source(String source) {
      this.source = source;
      return this;
    }


    /**
     * Get source
     * @return source
     **/
    @ApiModelProperty(value = "")
    public String getSource() {
      return source;
    }
    public void setSource(String source) {
      this.source = source;
    }

    public FileInfo url(String url) {
      this.url = url;
      return this;
    }


    /**
     * Get url
     * @return url
     **/
    @ApiModelProperty(value = "")
    public String getUrl() {
      return url;
    }
    public void setUrl(String url) {
      this.url = url;
    }

    public FileInfo credits(String credits) {
      this.credits = credits;
      return this;
    }


    /**
     * Get credits
     * @return credits
     **/
    @ApiModelProperty(value = "")
    public String getCredits() {
      return credits;
    }
    public void setCredits(String credits) {
      this.credits = credits;
    }

    public FileInfo duration(String duration) {
      this.duration = duration;
      return this;
    }


    /**
     * Get duration
     * @return duration
     **/
    @ApiModelProperty(value = "")
    public String getDuration() {
      return duration;
    }
    public void setDuration(String duration) {
      this.duration = duration;
    }

    public FileInfo time(String time) {
      this.time = time;
      return this;
    }


    /**
     * Get time
     * @return time
     **/
    @ApiModelProperty(value = "")
    public String getTime() {
      return time;
    }
    public void setTime(String time) {
      this.time = time;
    }

    public FileInfo shareUrl(String shareUrl) {
      this.shareUrl = shareUrl;
      return this;
    }


    /**
     * Get shareUrl
     * @return shareUrl
     **/
    @ApiModelProperty(value = "")
    public String getShareUrl() {
      return shareUrl;
    }
    public void setShareUrl(String shareUrl) {
      this.shareUrl = shareUrl;
    }

    public FileInfo downloadUrl(String downloadUrl) {
      this.downloadUrl = downloadUrl;
      return this;
    }


    /**
     * Get downloadUrl
     * @return downloadUrl
     **/
    @ApiModelProperty(value = "")
    public String getDownloadUrl() {
      return downloadUrl;
    }
    public void setDownloadUrl(String downloadUrl) {
      this.downloadUrl = downloadUrl;
    }

    @Override
    public boolean equals(java.lang.Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      FileInfo FileInfo = (FileInfo) o;
      return Objects.equals(this.id, FileInfo.id) &&
              Objects.equals(this.accessNumber, FileInfo.accessNumber) &&
              Objects.equals(this.name, FileInfo.name) &&
              Objects.equals(this.fName, FileInfo.fName) &&
              Objects.equals(this.lName, FileInfo.lName) &&
              Objects.equals(this.email, FileInfo.email) &&
              Objects.equals(this.phone, FileInfo.phone) &&
              Objects.equals(this.notes, FileInfo.notes) &&
              Objects.equals(this.meta, FileInfo.meta) &&
              Objects.equals(this.source, FileInfo.source) &&
              Objects.equals(this.url, FileInfo.url) &&
              Objects.equals(this.credits, FileInfo.credits) &&
              Objects.equals(this.duration, FileInfo.duration) &&
              Objects.equals(this.time, FileInfo.time) &&
              Objects.equals(this.shareUrl, FileInfo.shareUrl) &&
              Objects.equals(this.downloadUrl, FileInfo.downloadUrl);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id, accessNumber, name, fName, lName, email, phone, notes, meta, source, url, credits, duration, time, shareUrl, downloadUrl);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class FileInfo {\n");

      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    accessNumber: ").append(toIndentedString(accessNumber)).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    fName: ").append(toIndentedString(fName)).append("\n");
      sb.append("    lName: ").append(toIndentedString(lName)).append("\n");
      sb.append("    email: ").append(toIndentedString(email)).append("\n");
      sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
      sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
      sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
      sb.append("    source: ").append(toIndentedString(source)).append("\n");
      sb.append("    url: ").append(toIndentedString(url)).append("\n");
      sb.append("    credits: ").append(toIndentedString(credits)).append("\n");
      sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
      sb.append("    time: ").append(toIndentedString(time)).append("\n");
      sb.append("    shareUrl: ").append(toIndentedString(shareUrl)).append("\n");
      sb.append("    downloadUrl: ").append(toIndentedString(downloadUrl)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }

  }

}



