package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateFolderRequest
 */
public class UpdateFolderRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("id")
  private Long id = null;
  
  @SerializedName("name")
  private String name = null;
  
  @SerializedName("pass")
  private String pass = null;
  
  @SerializedName("is_private")
  private Boolean isPrivate = null;
  
  public UpdateFolderRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateFolderRequest id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "464", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public UpdateFolderRequest name(String name) {
    this.name = name;
    return this;
  }

  
  /**
  * Get name
  * @return name
  **/
  @ApiModelProperty(example = "testname", value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public UpdateFolderRequest pass(String pass) {
    this.pass = pass;
    return this;
  }

  
  /**
  * Get pass
  * @return pass
  **/
  @ApiModelProperty(example = "testpass", value = "")
  public String getPass() {
    return pass;
  }
  public void setPass(String pass) {
    this.pass = pass;
  }
  
  public UpdateFolderRequest isPrivate(Boolean isPrivate) {
    this.isPrivate = isPrivate;
    return this;
  }

  
  /**
  * Get isPrivate
  * @return isPrivate
  **/
  @ApiModelProperty(example = "false", value = "")
  public Boolean isIsPrivate() {
    return isPrivate;
  }
  public void setIsPrivate(Boolean isPrivate) {
    this.isPrivate = isPrivate;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateFolderRequest updateFolderRequest = (UpdateFolderRequest) o;
    return Objects.equals(this.apiKey, updateFolderRequest.apiKey) &&
        Objects.equals(this.id, updateFolderRequest.id) &&
        Objects.equals(this.name, updateFolderRequest.name) &&
        Objects.equals(this.pass, updateFolderRequest.pass) &&
        Objects.equals(this.isPrivate, updateFolderRequest.isPrivate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, id, name, pass, isPrivate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateFolderRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    pass: ").append(toIndentedString(pass)).append("\n");
    sb.append("    isPrivate: ").append(toIndentedString(isPrivate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



