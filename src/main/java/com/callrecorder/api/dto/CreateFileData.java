package com.callrecorder.api.dto;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * CreateFileData
 */
public class CreateFileData {

  @SerializedName("name")
  private String name = null;
  
  @SerializedName("email")
  private String email = null;
  
  @SerializedName("phone")
  private String phone = null;
  
  @SerializedName("l_name")
  private String lName = null;
  
  @SerializedName("f_name")
  private String fName = null;
  
  @SerializedName("notes")
  private String notes = null;
  
  @SerializedName("tags")
  private String tags = null;
  
  @SerializedName("meta")
  private List<String> meta = null;
  
  /**
   * Gets or Sets source
   */
  @JsonAdapter(SourceEnum.Adapter.class)
  public enum SourceEnum {
    
    _0("0"),
    _1("1");

    private String value;

    SourceEnum(String value) {
      this.value = value;
    }
    
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    
    public static SourceEnum fromValue(String text) {
      for (SourceEnum b : SourceEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
    
    public static class Adapter extends TypeAdapter<SourceEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final SourceEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public SourceEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return SourceEnum.fromValue(String.valueOf(value));
      }
    }
  }
  
  @SerializedName("source")
  private SourceEnum source = null;
  
  @SerializedName("remind_days")
  private String remindDays = null;
  
  @SerializedName("remind_date")
  private OffsetDateTime remindDate = null;
  
  public CreateFileData name(String name) {
    this.name = name;
    return this;
  }

  
  /**
  * Get name
  * @return name
  **/
  @ApiModelProperty(example = "testfile", value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public CreateFileData email(String email) {
    this.email = email;
    return this;
  }

  
  /**
  * Get email
  * @return email
  **/
  @ApiModelProperty(example = "test@mail.com", value = "")
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  
  public CreateFileData phone(String phone) {
    this.phone = phone;
    return this;
  }

  
  /**
  * Get phone
  * @return phone
  **/
  @ApiModelProperty(example = "+16463742122", value = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public CreateFileData lName(String lName) {
    this.lName = lName;
    return this;
  }

  
  /**
  * Get lName
  * @return lName
  **/
  @ApiModelProperty(example = "testlname", value = "")
  public String getLName() {
    return lName;
  }
  public void setLName(String lName) {
    this.lName = lName;
  }
  
  public CreateFileData fName(String fName) {
    this.fName = fName;
    return this;
  }

  
  /**
  * Get fName
  * @return fName
  **/
  @ApiModelProperty(example = "testfname", value = "")
  public String getFName() {
    return fName;
  }
  public void setFName(String fName) {
    this.fName = fName;
  }
  
  public CreateFileData notes(String notes) {
    this.notes = notes;
    return this;
  }

  
  /**
  * Get notes
  * @return notes
  **/
  @ApiModelProperty(example = "testnotes", value = "")
  public String getNotes() {
    return notes;
  }
  public void setNotes(String notes) {
    this.notes = notes;
  }
  
  public CreateFileData tags(String tags) {
    this.tags = tags;
    return this;
  }

  
  /**
  * Get tags
  * @return tags
  **/
  @ApiModelProperty(example = "testtags", value = "")
  public String getTags() {
    return tags;
  }
  public void setTags(String tags) {
    this.tags = tags;
  }
  
  public CreateFileData meta(List<String> meta) {
    this.meta = meta;
    return this;
  }

  public CreateFileData addMetaItem(String metaItem) {
    
    if (this.meta == null) {
      this.meta = new ArrayList<>();
    }
    
    this.meta.add(metaItem);
    return this;
  }
  
  /**
  * Get meta
  * @return meta
  **/
  @ApiModelProperty(value = "")
  public List<String> getMeta() {
    return meta;
  }
  public void setMeta(List<String> meta) {
    this.meta = meta;
  }
  
  public CreateFileData source(SourceEnum source) {
    this.source = source;
    return this;
  }

  
  /**
  * Get source
  * @return source
  **/
  @ApiModelProperty(example = "0", value = "")
  public SourceEnum getSource() {
    return source;
  }
  public void setSource(SourceEnum source) {
    this.source = source;
  }
  
  public CreateFileData remindDays(String remindDays) {
    this.remindDays = remindDays;
    return this;
  }

  
  /**
  * Get remindDays
  * @return remindDays
  **/
  @ApiModelProperty(example = "10", value = "")
  public String getRemindDays() {
    return remindDays;
  }
  public void setRemindDays(String remindDays) {
    this.remindDays = remindDays;
  }
  
  public CreateFileData remindDate(OffsetDateTime remindDate) {
    this.remindDate = remindDate;
    return this;
  }

  
  /**
  * Get remindDate
  * @return remindDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getRemindDate() {
    return remindDate;
  }
  public void setRemindDate(OffsetDateTime remindDate) {
    this.remindDate = remindDate;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateFileData createFileData = (CreateFileData) o;
    return Objects.equals(this.name, createFileData.name) &&
        Objects.equals(this.email, createFileData.email) &&
        Objects.equals(this.phone, createFileData.phone) &&
        Objects.equals(this.lName, createFileData.lName) &&
        Objects.equals(this.fName, createFileData.fName) &&
        Objects.equals(this.notes, createFileData.notes) &&
        Objects.equals(this.tags, createFileData.tags) &&
        Objects.equals(this.meta, createFileData.meta) &&
        Objects.equals(this.source, createFileData.source) &&
        Objects.equals(this.remindDays, createFileData.remindDays) &&
        Objects.equals(this.remindDate, createFileData.remindDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, email, phone, lName, fName, notes, tags, meta, source, remindDays, remindDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateFileData {\n");

    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    lName: ").append(toIndentedString(lName)).append("\n");
    sb.append("    fName: ").append(toIndentedString(fName)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    remindDays: ").append(toIndentedString(remindDays)).append("\n");
    sb.append("    remindDate: ").append(toIndentedString(remindDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



