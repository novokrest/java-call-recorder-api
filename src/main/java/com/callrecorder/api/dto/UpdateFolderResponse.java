package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateFolderResponse
 */
public class UpdateFolderResponse {

    @SerializedName("status")
    private String status = null;

    @SerializedName("msg")
    private String msg = null;

    @SerializedName("code")
    private String code = null;

    public UpdateFolderResponse status(String status) {
        this.status = status;
        return this;
    }


    /**
     * Get status
     * @return status
     **/
    @ApiModelProperty(example = "ok", required = true, value = "")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public UpdateFolderResponse msg(String msg) {
        this.msg = msg;
        return this;
    }


    /**
     * Get msg
     * @return msg
     **/
    @ApiModelProperty(example = "Successfully", value = "")
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UpdateFolderResponse code(String code) {
        this.code = code;
        return this;
    }


    /**
     * Get code
     * @return code
     **/
    @ApiModelProperty(example = "folder_updated", value = "")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UpdateFolderResponse updateFolderResponse = (UpdateFolderResponse) o;
        return Objects.equals(this.status, updateFolderResponse.status) &&
                Objects.equals(this.msg, updateFolderResponse.msg) &&
                Objects.equals(this.code, updateFolderResponse.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, msg, code);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UpdateFolderResponse {\n");

        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
        sb.append("    code: ").append(toIndentedString(code)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }


}
