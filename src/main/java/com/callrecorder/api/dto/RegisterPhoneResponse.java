package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RegisterPhoneResponse
 */
public class RegisterPhoneResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("phone")
  private String phone = null;
  
  @SerializedName("code")
  private String code = null;
  
  @SerializedName("msg")
  private String msg = null;
  
  public RegisterPhoneResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public RegisterPhoneResponse phone(String phone) {
    this.phone = phone;
    return this;
  }

  
  /**
  * Get phone
  * @return phone
  **/
  @ApiModelProperty(example = "+16463742122", value = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public RegisterPhoneResponse code(String code) {
    this.code = code;
    return this;
  }

  
  /**
  * Get code
  * @return code
  **/
  @ApiModelProperty(example = "54004", value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }
  
  public RegisterPhoneResponse msg(String msg) {
    this.msg = msg;
    return this;
  }

  
  /**
  * Get msg
  * @return msg
  **/
  @ApiModelProperty(example = "Verification Code Sent", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterPhoneResponse registerPhoneResponse = (RegisterPhoneResponse) o;
    return Objects.equals(this.status, registerPhoneResponse.status) &&
        Objects.equals(this.phone, registerPhoneResponse.phone) &&
        Objects.equals(this.code, registerPhoneResponse.code) &&
        Objects.equals(this.msg, registerPhoneResponse.msg);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, phone, code, msg);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegisterPhoneResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



