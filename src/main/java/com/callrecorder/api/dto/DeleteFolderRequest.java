package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * DeleteFolderRequest
 */
public class DeleteFolderRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("id")
  private Long id = null;
  
  @SerializedName("move_to")
  private Long moveTo = null;
  
  public DeleteFolderRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public DeleteFolderRequest id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "464", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public DeleteFolderRequest moveTo(Long moveTo) {
    this.moveTo = moveTo;
    return this;
  }

  
  /**
  * Get moveTo
  * @return moveTo
  **/
  @ApiModelProperty(example = "464", value = "")
  public Long getMoveTo() {
    return moveTo;
  }
  public void setMoveTo(Long moveTo) {
    this.moveTo = moveTo;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeleteFolderRequest deleteFolderRequest = (DeleteFolderRequest) o;
    return Objects.equals(this.apiKey, deleteFolderRequest.apiKey) &&
        Objects.equals(this.id, deleteFolderRequest.id) &&
        Objects.equals(this.moveTo, deleteFolderRequest.moveTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, id, moveTo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeleteFolderRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    moveTo: ").append(toIndentedString(moveTo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



