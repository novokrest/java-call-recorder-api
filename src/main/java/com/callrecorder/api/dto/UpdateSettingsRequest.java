package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateSettingsRequest
 */
public class UpdateSettingsRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("play_beep")
  private PlayBeep playBeep = null;
  
  @SerializedName("files_permission")
  private FilesPermission filesPermission = null;
  
  public UpdateSettingsRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public UpdateSettingsRequest playBeep(PlayBeep playBeep) {
    this.playBeep = playBeep;
    return this;
  }

  
  /**
  * Get playBeep
  * @return playBeep
  **/
  @ApiModelProperty(value = "")
  public PlayBeep getPlayBeep() {
    return playBeep;
  }
  public void setPlayBeep(PlayBeep playBeep) {
    this.playBeep = playBeep;
  }
  
  public UpdateSettingsRequest filesPermission(FilesPermission filesPermission) {
    this.filesPermission = filesPermission;
    return this;
  }

  
  /**
  * Get filesPermission
  * @return filesPermission
  **/
  @ApiModelProperty(value = "")
  public FilesPermission getFilesPermission() {
    return filesPermission;
  }
  public void setFilesPermission(FilesPermission filesPermission) {
    this.filesPermission = filesPermission;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateSettingsRequest updateSettingsRequest = (UpdateSettingsRequest) o;
    return Objects.equals(this.apiKey, updateSettingsRequest.apiKey) &&
        Objects.equals(this.playBeep, updateSettingsRequest.playBeep) &&
        Objects.equals(this.filesPermission, updateSettingsRequest.filesPermission);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, playBeep, filesPermission);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateSettingsRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    playBeep: ").append(toIndentedString(playBeep)).append("\n");
    sb.append("    filesPermission: ").append(toIndentedString(filesPermission)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



