package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * RecoverFileRequest
 */
public class RecoverFileRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("id")
  private Long id = null;
  
  @SerializedName("folder_id")
  private Long folderId = null;
  
  public RecoverFileRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public RecoverFileRequest id(Long id) {
    this.id = id;
    return this;
  }

  
  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  
  public RecoverFileRequest folderId(Long folderId) {
    this.folderId = folderId;
    return this;
  }

  
  /**
  * Get folderId
  * @return folderId
  **/
  @ApiModelProperty(example = "10", value = "")
  public Long getFolderId() {
    return folderId;
  }
  public void setFolderId(Long folderId) {
    this.folderId = folderId;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RecoverFileRequest recoverFileRequest = (RecoverFileRequest) o;
    return Objects.equals(this.apiKey, recoverFileRequest.apiKey) &&
        Objects.equals(this.id, recoverFileRequest.id) &&
        Objects.equals(this.folderId, recoverFileRequest.folderId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, id, folderId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RecoverFileRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    folderId: ").append(toIndentedString(folderId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



