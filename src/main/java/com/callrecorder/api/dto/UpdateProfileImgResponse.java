package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateProfileImgResponse
 */
public class UpdateProfileImgResponse {

    @SerializedName("status")
    private String status = null;

    @SerializedName("msg")
    private String msg = null;

    @SerializedName("code")
    private String code = null;

    @SerializedName("file")
    private String file = null;

    @SerializedName("path")
    private String path = null;

    public UpdateProfileImgResponse status(String status) {
        this.status = status;
        return this;
    }


    /**
     * Get status
     * @return status
     **/
    @ApiModelProperty(example = "ok", required = true, value = "")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public UpdateProfileImgResponse msg(String msg) {
        this.msg = msg;
        return this;
    }


    /**
     * Get msg
     * @return msg
     **/
    @ApiModelProperty(example = "Profile Picture Updated", value = "")
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UpdateProfileImgResponse code(String code) {
        this.code = code;
        return this;
    }


    /**
     * Get code
     * @return code
     **/
    @ApiModelProperty(example = "profile_pic_updated", value = "")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public UpdateProfileImgResponse file(String file) {
        this.file = file;
        return this;
    }


    /**
     * Get file
     * @return file
     **/
    @ApiModelProperty(example = "message.jpg", value = "")
    public String getFile() {
        return file;
    }
    public void setFile(String file) {
        this.file = file;
    }

    public UpdateProfileImgResponse path(String path) {
        this.path = path;
        return this;
    }


    /**
     * Get path
     * @return path
     **/
    @ApiModelProperty(example = "https://vxphone.s3.us-east-2.amazonaws.com/5cc96676a9", value = "")
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UpdateProfileImgResponse updateProfileImgResponse = (UpdateProfileImgResponse) o;
        return Objects.equals(this.status, updateProfileImgResponse.status) &&
                Objects.equals(this.msg, updateProfileImgResponse.msg) &&
                Objects.equals(this.code, updateProfileImgResponse.code) &&
                Objects.equals(this.file, updateProfileImgResponse.file) &&
                Objects.equals(this.path, updateProfileImgResponse.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, msg, code, file, path);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UpdateProfileImgResponse {\n");

        sb.append("    status: ").append(toIndentedString(status)).append("\n");
        sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
        sb.append("    code: ").append(toIndentedString(code)).append("\n");
        sb.append("    file: ").append(toIndentedString(file)).append("\n");
        sb.append("    path: ").append(toIndentedString(path)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }


}



