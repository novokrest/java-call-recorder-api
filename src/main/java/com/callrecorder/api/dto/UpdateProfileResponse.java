package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateProfileResponse
 */
public class UpdateProfileResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("msg")
  private String msg = null;
  
  @SerializedName("code")
  private String code = null;
  
  public UpdateProfileResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public UpdateProfileResponse msg(String msg) {
    this.msg = msg;
    return this;
  }

  
  /**
  * Get msg
  * @return msg
  **/
  @ApiModelProperty(example = "Profile Updated", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  public UpdateProfileResponse code(String code) {
    this.code = code;
    return this;
  }

  
  /**
  * Get code
  * @return code
  **/
  @ApiModelProperty(example = "profile_updated", value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateProfileResponse updateProfileResponse = (UpdateProfileResponse) o;
    return Objects.equals(this.status, updateProfileResponse.status) &&
        Objects.equals(this.msg, updateProfileResponse.msg) &&
        Objects.equals(this.code, updateProfileResponse.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, msg, code);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateProfileResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



