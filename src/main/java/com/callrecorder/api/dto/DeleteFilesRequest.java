package com.callrecorder.api.dto;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * DeleteFilesRequest
 */
public class DeleteFilesRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("ids")
  private List<Long> ids = new ArrayList<>();
  
  /**
   * Gets or Sets action
   */
  @JsonAdapter(ActionEnum.Adapter.class)
  public enum ActionEnum {
    
    FOREVER("remove_forever");

    private String value;

    ActionEnum(String value) {
      this.value = value;
    }
    
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
    
    public static ActionEnum fromValue(String text) {
      for (ActionEnum b : ActionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
    
    public static class Adapter extends TypeAdapter<ActionEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final ActionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public ActionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return ActionEnum.fromValue(String.valueOf(value));
      }
    }
  }
  
  @SerializedName("action")
  private ActionEnum action = null;
  
  public DeleteFilesRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public DeleteFilesRequest ids(List<Long> ids) {
    this.ids = ids;
    return this;
  }

  public DeleteFilesRequest addIdsItem(Long idsItem) {
    
    this.ids.add(idsItem);
    return this;
  }
  
  /**
  * Get ids
  * @return ids
  **/
  @ApiModelProperty(required = true, value = "")
  public List<Long> getIds() {
    return ids;
  }
  public void setIds(List<Long> ids) {
    this.ids = ids;
  }
  
  public DeleteFilesRequest action(ActionEnum action) {
    this.action = action;
    return this;
  }

  
  /**
  * Get action
  * @return action
  **/
  @ApiModelProperty(example = "remove_forever", value = "")
  public ActionEnum getAction() {
    return action;
  }
  public void setAction(ActionEnum action) {
    this.action = action;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeleteFilesRequest deleteFilesRequest = (DeleteFilesRequest) o;
    return Objects.equals(this.apiKey, deleteFilesRequest.apiKey) &&
        Objects.equals(this.ids, deleteFilesRequest.ids) &&
        Objects.equals(this.action, deleteFilesRequest.action);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, ids, action);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeleteFilesRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    ids: ").append(toIndentedString(ids)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



