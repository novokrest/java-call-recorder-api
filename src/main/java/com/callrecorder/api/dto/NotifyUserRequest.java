package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * NotifyUserRequest
 */
public class NotifyUserRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("title")
  private String title = null;
  
  @SerializedName("body")
  private String body = null;
  
  @SerializedName("device_type")
  private DeviceType deviceType = null;
  
  public NotifyUserRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public NotifyUserRequest title(String title) {
    this.title = title;
    return this;
  }

  
  /**
  * Get title
  * @return title
  **/
  @ApiModelProperty(example = "test-title", value = "")
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  
  public NotifyUserRequest body(String body) {
    this.body = body;
    return this;
  }

  
  /**
  * Get body
  * @return body
  **/
  @ApiModelProperty(example = "test-body", value = "")
  public String getBody() {
    return body;
  }
  public void setBody(String body) {
    this.body = body;
  }
  
  public NotifyUserRequest deviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
    return this;
  }

  
  /**
  * Get deviceType
  * @return deviceType
  **/
  @ApiModelProperty(value = "")
  public DeviceType getDeviceType() {
    return deviceType;
  }
  public void setDeviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotifyUserRequest notifyUserRequest = (NotifyUserRequest) o;
    return Objects.equals(this.apiKey, notifyUserRequest.apiKey) &&
        Objects.equals(this.title, notifyUserRequest.title) &&
        Objects.equals(this.body, notifyUserRequest.body) &&
        Objects.equals(this.deviceType, notifyUserRequest.deviceType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, title, body, deviceType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotifyUserRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("    deviceType: ").append(toIndentedString(deviceType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



