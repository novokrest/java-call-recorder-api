package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * VerifyPhoneRequest
 */
public class VerifyPhoneRequest {

  @SerializedName("token")
  private String token = null;

  @SerializedName("phone")
  private String phone = null;

  @SerializedName("code")
  private String code = null;

  @SerializedName("mcc")
  private String mcc = null;

  @SerializedName("app")
  private App app = null;

  @SerializedName("device_token")
  private String deviceToken = null;

  @SerializedName("device_id")
  private String deviceId = null;

  @SerializedName("device_type")
  private DeviceType deviceType = null;

  @SerializedName("time_zone")
  private String timeZone = null;

  public VerifyPhoneRequest token(String token) {
    this.token = token;
    return this;
  }


  /**
   * Get token
   * @return token
   **/
  @ApiModelProperty(example = "55942ee3894f51000530894", required = true, value = "")
  public String getToken() {
    return token;
  }
  public void setToken(String token) {
    this.token = token;
  }

  public VerifyPhoneRequest phone(String phone) {
    this.phone = phone;
    return this;
  }


  /**
   * Get phone
   * @return phone
   **/
  @ApiModelProperty(example = "+16463742122", required = true, value = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }

  public VerifyPhoneRequest code(String code) {
    this.code = code;
    return this;
  }


  /**
   * Get code
   * @return code
   **/
  @ApiModelProperty(example = "54004", required = true, value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  public VerifyPhoneRequest mcc(String mcc) {
    this.mcc = mcc;
    return this;
  }


  /**
   * Get mcc
   * @return mcc
   **/
  @ApiModelProperty(example = "300", required = true, value = "")
  public String getMcc() {
    return mcc;
  }
  public void setMcc(String mcc) {
    this.mcc = mcc;
  }

  public VerifyPhoneRequest app(App app) {
    this.app = app;
    return this;
  }


  /**
   * Get app
   * @return app
   **/
  @ApiModelProperty(example = "free", required = true, value = "")
  public App getApp() {
    return app;
  }
  public void setApp(App app) {
    this.app = app;
  }

  public VerifyPhoneRequest deviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
    return this;
  }


  /**
   * Get deviceToken
   * @return deviceToken
   **/
  @ApiModelProperty(example = "234", required = true, value = "")
  public String getDeviceToken() {
    return deviceToken;
  }
  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }

  public VerifyPhoneRequest deviceId(String deviceId) {
    this.deviceId = deviceId;
    return this;
  }


  /**
   * Get deviceId
   * @return deviceId
   **/
  @ApiModelProperty(example = "234", value = "")
  public String getDeviceId() {
    return deviceId;
  }
  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public VerifyPhoneRequest deviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
    return this;
  }


  /**
   * Get deviceType
   * @return deviceType
   **/
  @ApiModelProperty(example = "android", required = true, value = "")
  public DeviceType getDeviceType() {
    return deviceType;
  }
  public void setDeviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
  }

  public VerifyPhoneRequest timeZone(String timeZone) {
    this.timeZone = timeZone;
    return this;
  }


  /**
   * Get timeZone
   * @return timeZone
   **/
  @ApiModelProperty(example = "10", required = true, value = "")
  public String getTimeZone() {
    return timeZone;
  }
  public void setTimeZone(String timeZone) {
    this.timeZone = timeZone;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VerifyPhoneRequest verifyPhoneRequest = (VerifyPhoneRequest) o;
    return Objects.equals(this.token, verifyPhoneRequest.token) &&
            Objects.equals(this.phone, verifyPhoneRequest.phone) &&
            Objects.equals(this.code, verifyPhoneRequest.code) &&
            Objects.equals(this.mcc, verifyPhoneRequest.mcc) &&
            Objects.equals(this.app, verifyPhoneRequest.app) &&
            Objects.equals(this.deviceToken, verifyPhoneRequest.deviceToken) &&
            Objects.equals(this.deviceId, verifyPhoneRequest.deviceId) &&
            Objects.equals(this.deviceType, verifyPhoneRequest.deviceType) &&
            Objects.equals(this.timeZone, verifyPhoneRequest.timeZone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(token, phone, code, mcc, app, deviceToken, deviceId, deviceType, timeZone);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VerifyPhoneRequest {\n");

    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    mcc: ").append(toIndentedString(mcc)).append("\n");
    sb.append("    app: ").append(toIndentedString(app)).append("\n");
    sb.append("    deviceToken: ").append(toIndentedString(deviceToken)).append("\n");
    sb.append("    deviceId: ").append(toIndentedString(deviceId)).append("\n");
    sb.append("    deviceType: ").append(toIndentedString(deviceType)).append("\n");
    sb.append("    timeZone: ").append(toIndentedString(timeZone)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}