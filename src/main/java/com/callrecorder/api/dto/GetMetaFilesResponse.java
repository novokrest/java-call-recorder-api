package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * GetMetaFilesResponse
 */
public class GetMetaFilesResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("meta_files")
  private List<MetaFileInfo> metaFiles = null;
  
  public GetMetaFilesResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public GetMetaFilesResponse metaFiles(List<MetaFileInfo> metaFiles) {
    this.metaFiles = metaFiles;
    return this;
  }

  public GetMetaFilesResponse addMetaFilesItem(MetaFileInfo metaFilesItem) {
    
    if (this.metaFiles == null) {
      this.metaFiles = new ArrayList<>();
    }
    
    this.metaFiles.add(metaFilesItem);
    return this;
  }
  
  /**
  * Get metaFiles
  * @return metaFiles
  **/
  @ApiModelProperty(value = "")
  public List<MetaFileInfo> getMetaFiles() {
    return metaFiles;
  }
  public void setMetaFiles(List<MetaFileInfo> metaFiles) {
    this.metaFiles = metaFiles;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetMetaFilesResponse getMetaFilesResponse = (GetMetaFilesResponse) o;
    return Objects.equals(this.status, getMetaFilesResponse.status) &&
        Objects.equals(this.metaFiles, getMetaFilesResponse.metaFiles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, metaFiles);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetMetaFilesResponse {\n");

    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    metaFiles: ").append(toIndentedString(metaFiles)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class MetaFileInfo {

    @SerializedName("id")
    private Long id = null;

    @SerializedName("parent_id")
    private Long parentId = null;

    @SerializedName("name")
    private String name = null;

    @SerializedName("file")
    private String file = null;

    @SerializedName("user_id")
    private Long userId = null;

    @SerializedName("time")
    private LocalDateTime time = null;

    public MetaFileInfo id(Long id) {
      this.id = id;
      return this;
    }


    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "577", value = "")
    public Long getId() {
      return id;
    }
    public void setId(Long id) {
      this.id = id;
    }

    public MetaFileInfo parentId(Long parentId) {
      this.parentId = parentId;
      return this;
    }


    /**
     * Get parentId
     * @return parentId
     **/
    @ApiModelProperty(example = "577", value = "")
    public Long getParentId() {
      return parentId;
    }
    public void setParentId(Long parentId) {
      this.parentId = parentId;
    }

    public MetaFileInfo name(String name) {
      this.name = name;
      return this;
    }


    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "test-name", value = "")
    public String getName() {
      return name;
    }
    public void setName(String name) {
      this.name = name;
    }

    public MetaFileInfo file(String file) {
      this.file = file;
      return this;
    }


    /**
     * Get file
     * @return file
     **/
    @ApiModelProperty(example = "test-file", value = "")
    public String getFile() {
      return file;
    }
    public void setFile(String file) {
      this.file = file;
    }

    public MetaFileInfo userId(Long userId) {
      this.userId = userId;
      return this;
    }


    /**
     * Get userId
     * @return userId
     **/
    @ApiModelProperty(example = "577", value = "")
    public Long getUserId() {
      return userId;
    }
    public void setUserId(Long userId) {
      this.userId = userId;
    }

    public MetaFileInfo time(LocalDateTime time) {
      this.time = time;
      return this;
    }


    /**
     * Get time
     * @return time
     **/
    @ApiModelProperty(value = "")
    public LocalDateTime getTime() {
      return time;
    }
    public void setTime(LocalDateTime time) {
      this.time = time;
    }

    @Override
    public boolean equals(java.lang.Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      MetaFileInfo MetaFileInfo = (MetaFileInfo) o;
      return Objects.equals(this.id, MetaFileInfo.id) &&
              Objects.equals(this.parentId, MetaFileInfo.parentId) &&
              Objects.equals(this.name, MetaFileInfo.name) &&
              Objects.equals(this.file, MetaFileInfo.file) &&
              Objects.equals(this.userId, MetaFileInfo.userId) &&
              Objects.equals(this.time, MetaFileInfo.time);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id, parentId, name, file, userId, time);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class MetaFileInfo {\n");

      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    parentId: ").append(toIndentedString(parentId)).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    file: ").append(toIndentedString(file)).append("\n");
      sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
      sb.append("    time: ").append(toIndentedString(time)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }
    
  }

}



