package com.callrecorder.api.dto;


import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;


/**
 * Gets or Sets App
 */
@JsonAdapter(App.Adapter.class)
public enum App {
  
  FREE("free"),
  
  REM("rem"),
  
  REC("rec");

  private String value;

  App(String value) {
    this.value = value;
  }


  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }


  public static App fromValue(String text) {
    for (App b : App.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }


  public static class Adapter extends TypeAdapter<App> {
    @Override
    public void write(final JsonWriter jsonWriter, final App enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public App read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return App.fromValue(String.valueOf(value));
    }
  }

}



