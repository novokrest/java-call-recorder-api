package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * GetFoldersResponse
 */
public class GetFoldersResponse {

  @SerializedName("status")
  private String status = null;
  
  @SerializedName("msg")
  private String msg = null;
  
  @SerializedName("folders")
  private List<FileInfo> folders = null;
  
  public GetFoldersResponse status(String status) {
    this.status = status;
    return this;
  }

  
  /**
  * Get status
  * @return status
  **/
  @ApiModelProperty(example = "ok", required = true, value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
  public GetFoldersResponse msg(String msg) {
    this.msg = msg;
    return this;
  }

  
  /**
  * Get msg
  * @return msg
  **/
  @ApiModelProperty(example = "Success", value = "")
  public String getMsg() {
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }
  
  public GetFoldersResponse folders(List<FileInfo> folders) {
    this.folders = folders;
    return this;
  }

  public GetFoldersResponse addFoldersItem(FileInfo foldersItem) {
    
    if (this.folders == null) {
      this.folders = new ArrayList<>();
    }
    
    this.folders.add(foldersItem);
    return this;
  }
  
  /**
  * Get folders
  * @return folders
  **/
  @ApiModelProperty(value = "")
  public List<FileInfo> getFolders() {
    return folders;
  }
  public void setFolders(List<FileInfo> folders) {
    this.folders = folders;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetFoldersResponse getFoldersResponse = (GetFoldersResponse) o;
    return Objects.equals(this.status, getFoldersResponse.status) &&
        Objects.equals(this.msg, getFoldersResponse.msg) &&
        Objects.equals(this.folders, getFoldersResponse.folders);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, msg, folders);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetFoldersResponse {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    msg: ").append(toIndentedString(msg)).append("\n");
    sb.append("    folders: ").append(toIndentedString(folders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public static class FileInfo {


    @SerializedName("id")
    private Long id = null;

    @SerializedName("name")
    private String name = null;

    @SerializedName("created")
    private Long created = null;

    @SerializedName("updated")
    private Long updated = null;

    @SerializedName("is_start")
    private Integer isStart = null;

    @SerializedName("order_id")
    private Long orderId = null;

    public FileInfo id(Long id) {
      this.id = id;
      return this;
    }


    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "464", value = "")
    public Long getId() {
      return id;
    }
    public void setId(Long id) {
      this.id = id;
    }

    public FileInfo name(String name) {
      this.name = name;
      return this;
    }


    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "testname", value = "")
    public String getName() {
      return name;
    }
    public void setName(String name) {
      this.name = name;
    }

    public FileInfo created(Long created) {
      this.created = created;
      return this;
    }


    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(example = "464", value = "")
    public Long getCreated() {
      return created;
    }
    public void setCreated(Long created) {
      this.created = created;
    }

    public FileInfo updated(Long updated) {
      this.updated = updated;
      return this;
    }


    /**
     * Get updated
     * @return updated
     **/
    @ApiModelProperty(example = "464", value = "")
    public Long getUpdated() {
      return updated;
    }
    public void setUpdated(Long updated) {
      this.updated = updated;
    }

    public FileInfo isStart(Integer isStart) {
      this.isStart = isStart;
      return this;
    }


    /**
     * Get isStart
     * @return isStart
     **/
    @ApiModelProperty(example = "0", value = "")
    public Integer getIsStart() {
      return isStart;
    }
    public void setIsStart(Integer isStart) {
      this.isStart = isStart;
    }

    public FileInfo orderId(Long orderId) {
      this.orderId = orderId;
      return this;
    }


    /**
     * Get orderId
     * @return orderId
     **/
    @ApiModelProperty(example = "464", value = "")
    public Long getOrderId() {
      return orderId;
    }
    public void setOrderId(Long orderId) {
      this.orderId = orderId;
    }

    @Override
    public boolean equals(java.lang.Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      FileInfo getFoldersResponseFolders = (FileInfo) o;
      return Objects.equals(this.id, getFoldersResponseFolders.id) &&
              Objects.equals(this.name, getFoldersResponseFolders.name) &&
              Objects.equals(this.created, getFoldersResponseFolders.created) &&
              Objects.equals(this.updated, getFoldersResponseFolders.updated) &&
              Objects.equals(this.isStart, getFoldersResponseFolders.isStart) &&
              Objects.equals(this.orderId, getFoldersResponseFolders.orderId);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id, name, created, updated, isStart, orderId);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class FileInfo {\n");

      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    created: ").append(toIndentedString(created)).append("\n");
      sb.append("    updated: ").append(toIndentedString(updated)).append("\n");
      sb.append("    isStart: ").append(toIndentedString(isStart)).append("\n");
      sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
      sb.append("}");
      return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
      if (o == null) {
        return "null";
      }
      return o.toString().replace("\n", "\n    ");
    }

  }


}



