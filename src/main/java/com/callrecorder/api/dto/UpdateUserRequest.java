package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * UpdateUserRequest
 */
public class UpdateUserRequest {

  @SerializedName("api_key")
  private String apiKey = null;

  @SerializedName("app")
  private App app = null;

  @SerializedName("timezone")
  private String timezone = null;

  public UpdateUserRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }


  /**
   * Get apiKey
   * @return apiKey
   **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public UpdateUserRequest app(App app) {
    this.app = app;
    return this;
  }


  /**
   * Get app
   * @return app
   **/
  @ApiModelProperty(value = "")
  public App getApp() {
    return app;
  }
  public void setApp(App app) {
    this.app = app;
  }

  public UpdateUserRequest timezone(String timezone) {
    this.timezone = timezone;
    return this;
  }


  /**
   * Get timezone
   * @return timezone
   **/
  @ApiModelProperty(example = "10", value = "")
  public String getTimezone() {
    return timezone;
  }
  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UpdateUserRequest updateUserRequest = (UpdateUserRequest) o;
    return Objects.equals(this.apiKey, updateUserRequest.apiKey) &&
            Objects.equals(this.app, updateUserRequest.app) &&
            Objects.equals(this.timezone, updateUserRequest.timezone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, app, timezone);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UpdateUserRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    app: ").append(toIndentedString(app)).append("\n");
    sb.append("    timezone: ").append(toIndentedString(timezone)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}
