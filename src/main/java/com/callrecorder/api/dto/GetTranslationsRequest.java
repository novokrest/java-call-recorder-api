package com.callrecorder.api.dto;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 * GetTranslationsRequest
 */
public class GetTranslationsRequest {

  @SerializedName("api_key")
  private String apiKey = null;
  
  @SerializedName("language")
  private String language = null;
  
  public GetTranslationsRequest apiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  
  /**
  * Get apiKey
  * @return apiKey
  **/
  @ApiModelProperty(example = "57872b508520557872b50855c", required = true, value = "")
  public String getApiKey() {
    return apiKey;
  }
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public GetTranslationsRequest language(String language) {
    this.language = language;
    return this;
  }

  
  /**
  * Get language
  * @return language
  **/
  @ApiModelProperty(example = "en-us", value = "")
  public String getLanguage() {
    return language;
  }
  public void setLanguage(String language) {
    this.language = language;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetTranslationsRequest getTranslationsRequest = (GetTranslationsRequest) o;
    return Objects.equals(this.apiKey, getTranslationsRequest.apiKey) &&
        Objects.equals(this.language, getTranslationsRequest.language);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, language);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetTranslationsRequest {\n");

    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    language: ").append(toIndentedString(language)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  
}



