package com.callrecorder.api;

import com.callrecorder.api.client.ApiClient;

import javax.annotation.Nonnull;

public class CallRecorderClientFactory {

    @Nonnull
    public static CallRecorderApi createApiClient() {
        return new CallRecorderApi();
    }

    @Nonnull
    public static CallRecorderApi createApiClient(@Nonnull String baseUrl) {
        CallRecorderApi callRecorderApi = new CallRecorderApi();
        callRecorderApi.setApiClient(
                new ApiClient()
                        .setBasePath(baseUrl)
        );
        return callRecorderApi;
    }

    @Nonnull
    public static CallRecorderApi createApiClient(@Nonnull ApiClient apiClient) {
        CallRecorderApi callRecorderApi = new CallRecorderApi();
        callRecorderApi.setApiClient(apiClient);
        return callRecorderApi;
    }

}
