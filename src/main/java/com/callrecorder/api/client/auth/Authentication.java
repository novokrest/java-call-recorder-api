package com.callrecorder.api.client.auth;

import com.callrecorder.api.client.core.NameValuePair;

import java.util.List;
import java.util.Map;

public interface Authentication {
    /**
     * Apply authentication settings to header and query params.
     *
     * @param queryParams List of query parameters
     * @param headerParams Map of header parameters
     */
    void applyToParams(List<NameValuePair> queryParams, Map<String, String> headerParams);
}
