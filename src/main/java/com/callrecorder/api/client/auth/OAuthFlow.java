package com.callrecorder.api.client.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
