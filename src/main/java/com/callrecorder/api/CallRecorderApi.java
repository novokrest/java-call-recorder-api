package com.callrecorder.api;

import com.callrecorder.api.client.*;
import com.callrecorder.api.client.core.ProgressRequestBody;
import com.callrecorder.api.client.core.ProgressResponseBody;
import com.callrecorder.api.dto.*;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Call;
import org.apache.commons.lang3.tuple.Pair;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class CallRecorderApi {

    private enum Command {

        REGISTER_PHONE("/rapi/register_phone", "application/x-www-form-urlencoded"),

        VERIFY_PHONE("/rapi/verify_phone", "application/x-www-form-urlencoded"),

        GET_FILES("/rapi/get_files", "application/x-www-form-urlencoded"),

        CREATE_FILE("/rapi/create_file", "multipart/form-data"),

        DELETE_FILES("/rapi/delete_files", "application/x-www-form-urlencoded"),

        CREATE_FOLDER("/rapi/create_folder", "application/x-www-form-urlencoded"),

        UPDATE_FOLDER("/rapi/update_folder", "application/x-www-form-urlencoded"),

        VERIFY_FOLDER_PASS("/rapi/verify_folder_pass", "application/x-www-form-urlencoded"),

        DELETE_FOLDER("/rapi/delete_folder", "application/x-www-form-urlencoded"),

        GET_FOLDERS("/rapi/get_folders", "application/x-www-form-urlencoded"),

        UPDATE_STAR("/rapi/update_star", "application/x-www-form-urlencoded"),

        CLONE_FILE("/rapi/clone_file", "application/x-www-form-urlencoded"),

        UPDATE_PROFILE_IMG("/upload/update_profile_img", "multipart/form-data"),

        UPDATE_PROFILE("/rapi/update_profile", "application/x-www-form-urlencoded"),

        GET_PROFILE("/rapi/get_profile", "application/x-www-form-urlencoded"),

        UPDATE_ORDER("/rapi/update_order", "application/x-www-form-urlencoded"),

        RECOVER_FILE("/rapi/recover_file", "application/x-www-form-urlencoded"),

        UPDATE_SETTINGS("/rapi/update_settings", "application/x-www-form-urlencoded"),

        GET_SETTINGS("/rapi/get_settings", "application/x-www-form-urlencoded"),

        GET_MESSAGES("/rapi/get_msgs", "application/x-www-form-urlencoded"),

        BUY_CREDITS("/rapi/buy_credits", "application/x-www-form-urlencoded"),

        UPDATE_DEVICE_TOKEN("/rapi/update_device_token", "application/x-www-form-urlencoded"),

        GET_TRANSLATIONS("/rapi/get_translations", "application/x-www-form-urlencoded"),

        GET_LANGUAGES("/rapi/get_languages", "application/x-www-form-urlencoded"),

        GET_PHONES("/rapi/get_phones", "application/x-www-form-urlencoded"),

        UPDATE_USER("/rapi/update_user", "application/x-www-form-urlencoded"),

        NOTIFY_USER("/rapi/notify_user_custom", "application/x-www-form-urlencoded"),
        
        UPLOAD_META_FILE("/rapi/upload_meta_file", "multipart/form-data"),

        GET_META_FILES("/rapi/get_meta_files", "application/x-www-form-urlencoded"),

        DELETE_META_FILES("/rapi/delete_meta_files", "application/x-www-form-urlencoded"),

        ;

        private final String name;
        private final String contentType;

        Command(String name, String contentType) {
            this.name = name;
            this.contentType = contentType;
        }

    }

    private ApiClient apiClient;

    public CallRecorderApi() {
        this(Configuration.getDefaultApiClient());
    }

    public CallRecorderApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     *
     * Register Phone, Send phone number to server to get verification code
     * @param body  (required)
     * @return RegisterPhoneResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public RegisterPhoneResponse registerPhonePost(RegisterPhoneRequest body) throws ApiException {
        ApiResponse<RegisterPhoneResponse> resp = registerPhonePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     * Register Phone, Send phone number to server to get verification code
     * @param body  (required)
     * @return ApiResponse&lt;RegisterPhoneResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<RegisterPhoneResponse> registerPhonePostWithHttpInfo(RegisterPhoneRequest body) throws ApiException {
        com.squareup.okhttp.Call call = registerPhonePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<RegisterPhoneResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Register Phone, Send phone number to server to get verification code (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call registerPhonePostAsync(RegisterPhoneRequest body, final ApiCallback<RegisterPhoneResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = registerPhonePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<RegisterPhoneResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    public com.squareup.okhttp.Call registerPhonePostCall(
            RegisterPhoneRequest body,
            ProgressResponseBody.ProgressListener progressListener,
            ProgressRequestBody.ProgressRequestListener progressRequestListener
    ) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<String, Object>();
        formParams.put("token", body.getToken());
        formParams.put("phone", body.getPhone());

        return buildCall(
                Command.REGISTER_PHONE,
                formParams,
                progressListener,
                progressRequestListener
        );
    }

    /**
     *
     * Send phone number and verification code to get API Key
     * @param body  (required)
     * @return VerifyPhoneResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public VerifyPhoneResponse verifyPhonePost(VerifyPhoneRequest body) throws ApiException {
        ApiResponse<VerifyPhoneResponse> resp = verifyPhonePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Send phone number and verification code to get API Key
     * @param body  (required)
     * @return ApiResponse&lt;VerifyPhoneResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<VerifyPhoneResponse> verifyPhonePostWithHttpInfo(VerifyPhoneRequest body) throws ApiException {
        com.squareup.okhttp.Call call = verifyPhonePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<VerifyPhoneResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Send phone number and verification code to get API Key
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call verifyPhonePostAsync(VerifyPhoneRequest body, final ApiCallback<VerifyPhoneResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = verifyPhonePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<VerifyPhoneResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for verifyPhonePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call verifyPhonePostCall(VerifyPhoneRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<String, Object>();
        formParams.put("token", body.getToken());
        formParams.put("phone", body.getPhone());
        formParams.put("code", body.getCode());
        formParams.put("mcc", body.getMcc());
        formParams.put("app", body.getApp().getValue());
        formParams.put("device_type", body.getDeviceType().getValue());
        formParams.put("device_id", body.getDeviceId());
        formParams.put("device_token", body.getDeviceToken());
        formParams.put("time_zone", body.getTimeZone());

        return buildCall(Command.VERIFY_PHONE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Files
     * @param body  (required)
     * @return GetFilesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetFilesResponse getFilesPost(GetFilesRequest body) throws ApiException {
        ApiResponse<GetFilesResponse> resp = getFilesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Files
     * @param body  (required)
     * @return ApiResponse&lt;GetFilesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetFilesResponse> getFilesPostWithHttpInfo(GetFilesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getFilesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetFilesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Get Files
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getFilesPostAsync(GetFilesRequest body, final ApiCallback<GetFilesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getFilesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetFilesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getFilesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getFilesPostCall(GetFilesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<String, Object>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("page", body.getPage());
        formParams.put("folder_id", body.getFolderId());
        formParams.put("source", body.getSource().getValue());
        formParams.put("pass", body.getPass());
        formParams.put("reminder", body.isReminder());
        formParams.put("q", body.getQ());
        formParams.put("id", body.getId());
        formParams.put("op", body.getOp().getValue());

        return buildCall(Command.GET_FILES, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Create File
     * @param body  (required)
     * @return CreateFileResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public CreateFileResponse createFilePost(CreateFileRequest body) throws ApiException {
        ApiResponse<CreateFileResponse> resp = createFilePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Create File
     * @param body  (required)
     * @return ApiResponse&lt;CreateFileResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<CreateFileResponse> createFilePostWithHttpInfo(CreateFileRequest body) throws ApiException {
        com.squareup.okhttp.Call call = createFilePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<CreateFileResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create File (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call createFilePostAsync(CreateFileRequest body, final ApiCallback<CreateFileResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = createFilePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<CreateFileResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for createFilePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call createFilePostCall(CreateFileRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<String, Object>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("file", body.getFile());
        formParams.put("data", apiClient.getJSON().serialize(body.getData()));

        return buildCall(Command.CREATE_FILE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Delete Files
     * @param body  (required)
     * @return DeleteFilesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public DeleteFilesResponse deleteFilesPost(DeleteFilesRequest body) throws ApiException {
        ApiResponse<DeleteFilesResponse> resp = deleteFilesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Delete Files
     * @param body  (required)
     * @return ApiResponse&lt;DeleteFilesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<DeleteFilesResponse> deleteFilesPostWithHttpInfo(DeleteFilesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = deleteFilesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<DeleteFilesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete Files (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call deleteFilesPostAsync(DeleteFilesRequest body, final ApiCallback<DeleteFilesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = deleteFilesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<DeleteFilesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for deleteFilesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call deleteFilesPostCall(DeleteFilesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("ids", body.getIds());

        return buildCall(Command.DELETE_FILES, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Create Folder
     * @param body  (required)
     * @return CreateFolderResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public CreateFolderResponse createFolderPost(CreateFolderRequest body) throws ApiException {
        ApiResponse<CreateFolderResponse> resp = createFolderPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Create Folder
     * @param body  (required)
     * @return ApiResponse&lt;CreateFolderResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<CreateFolderResponse> createFolderPostWithHttpInfo(CreateFolderRequest body) throws ApiException {
        com.squareup.okhttp.Call call = createFolderPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<CreateFolderResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create Folder (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call createFolderPostAsync(CreateFolderRequest body, final ApiCallback<CreateFolderResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = createFolderPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<CreateFolderResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for createFolderPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call createFolderPostCall(CreateFolderRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("name", body.getName());
        formParams.put("pass", body.getPass());

        return buildCall(Command.CREATE_FOLDER, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Folder
     * @param body  (required)
     * @return UpdateFolderResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateFolderResponse updateFolderPost(UpdateFolderRequest body) throws ApiException {
        ApiResponse<UpdateFolderResponse> resp = updateFolderPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Folder
     * @param body  (required)
     * @return ApiResponse&lt;UpdateFolderResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateFolderResponse> updateFolderPostWithHttpInfo(UpdateFolderRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateFolderPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateFolderResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Folder (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateFolderPostAsync(UpdateFolderRequest body, final ApiCallback<UpdateFolderResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateFolderPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateFolderResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateFolderPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateFolderPostCall(UpdateFolderRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("id", body.getId());
        formParams.put("name", body.getName());
        formParams.put("pass", body.getPass());
        formParams.put("is_private", body.isIsPrivate());

        return buildCall(Command.UPDATE_FOLDER, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Verify Folder Pass
     * @param body  (required)
     * @return VerifyFolderPassResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public VerifyFolderPassResponse verifyFolderPassPost(VerifyFolderPassRequest body) throws ApiException {
        ApiResponse<VerifyFolderPassResponse> resp = verifyFolderPassPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Verify Folder Pass
     * @param body  (required)
     * @return ApiResponse&lt;VerifyFolderPassResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<VerifyFolderPassResponse> verifyFolderPassPostWithHttpInfo(VerifyFolderPassRequest body) throws ApiException {
        com.squareup.okhttp.Call call = verifyFolderPassPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<VerifyFolderPassResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Verify Folder Pass (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call verifyFolderPassPostAsync(VerifyFolderPassRequest body, final ApiCallback<VerifyFolderPassResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = verifyFolderPassPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<VerifyFolderPassResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for verifyFolderPassPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call verifyFolderPassPostCall(VerifyFolderPassRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("id", body.getId());
        formParams.put("pass", body.getPass());

        return buildCall(Command.VERIFY_FOLDER_PASS, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Delete Folder
     * @param body  (required)
     * @return DeleteFolderResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public DeleteFolderResponse deleteFolderPost(DeleteFolderRequest body) throws ApiException {
        ApiResponse<DeleteFolderResponse> resp = deleteFolderPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Delete Folder
     * @param body  (required)
     * @return ApiResponse&lt;DeleteFolderResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<DeleteFolderResponse> deleteFolderPostWithHttpInfo(DeleteFolderRequest body) throws ApiException {
        com.squareup.okhttp.Call call = deleteFolderPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<DeleteFolderResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete Folder (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call deleteFolderPostAsync(DeleteFolderRequest body, final ApiCallback<DeleteFolderResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = deleteFolderPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<DeleteFolderResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for deleteFolderPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call deleteFolderPostCall(DeleteFolderRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("id", body.getId());
        formParams.put("move_to", body.getMoveTo());

        return buildCall(Command.DELETE_FOLDER, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Folders
     * @param body  (required)
     * @return GetFoldersResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetFoldersResponse getFoldersPost(GetFoldersRequest body) throws ApiException {
        ApiResponse<GetFoldersResponse> resp = getFoldersPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Folders
     * @param body  (required)
     * @return ApiResponse&lt;GetFoldersResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetFoldersResponse> getFoldersPostWithHttpInfo(GetFoldersRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getFoldersPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetFoldersResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Folders (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getFoldersPostAsync(GetFoldersRequest body, final ApiCallback<GetFoldersResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getFoldersPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetFoldersResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getFoldersPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getFoldersPostCall(GetFoldersRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());

        return buildCall(Command.GET_FOLDERS, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Star
     * @param body  (required)
     * @return UpdateStarResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateStarResponse updateStarPost(UpdateStarRequest body) throws ApiException {
        ApiResponse<UpdateStarResponse> resp = updateStarPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Star
     * @param body  (required)
     * @return ApiResponse&lt;UpdateStarResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateStarResponse> updateStarPostWithHttpInfo(UpdateStarRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateStarPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateStarResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Star (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateStarPostAsync(UpdateStarRequest body, final ApiCallback<UpdateStarResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateStarPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateStarResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateStarPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateStarPostCall(UpdateStarRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("id", body.getId());
        formParams.put("type", body.getType().getValue());
        formParams.put("star", body.isStar() ? 1 : 0);

        return buildCall(Command.UPDATE_STAR, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Clone File
     * @param body  (required)
     * @return CloneFileResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public CloneFileResponse cloneFilePost(CloneFileRequest body) throws ApiException {
        ApiResponse<CloneFileResponse> resp = cloneFilePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Clone File
     * @param body  (required)
     * @return ApiResponse&lt;CloneFileResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<CloneFileResponse> cloneFilePostWithHttpInfo(CloneFileRequest body) throws ApiException {
        com.squareup.okhttp.Call call = cloneFilePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<CloneFileResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Clone File (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call cloneFilePostAsync(CloneFileRequest body, final ApiCallback<CloneFileResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = cloneFilePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<CloneFileResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for cloneFilePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call cloneFilePostCall(CloneFileRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("id", body.getId());

        return buildCall(Command.CLONE_FILE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Profile Img
     * @param body  (required)
     * @return UpdateProfileImgResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateProfileImgResponse updateProfileImgPost(UpdateProfileImgRequest body) throws ApiException {
        ApiResponse<UpdateProfileImgResponse> resp = updateProfileImgPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Profile Img
     * @param body  (required)
     * @return ApiResponse&lt;UpdateProfileImgResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateProfileImgResponse> updateProfileImgPostWithHttpInfo(UpdateProfileImgRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateProfileImgPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateProfileImgResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Profile Img (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateProfileImgPostAsync(UpdateProfileImgRequest body, final ApiCallback<UpdateProfileImgResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateProfileImgPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateProfileImgResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateProfileImgPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateProfileImgPostCall(UpdateProfileImgRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("file", body.getFile());

        return buildCall(Command.UPDATE_PROFILE_IMG, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Profile
     * @param body  (required)
     * @return UpdateProfileResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateProfileResponse updateProfilePost(UpdateProfileRequest body) throws ApiException {
        ApiResponse<UpdateProfileResponse> resp = updateProfilePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Profile
     * @param body  (required)
     * @return ApiResponse&lt;UpdateProfileResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateProfileResponse> updateProfilePostWithHttpInfo(UpdateProfileRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateProfilePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateProfileResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Profile (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateProfilePostAsync(UpdateProfileRequest body, final ApiCallback<UpdateProfileResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateProfilePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateProfileResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateProfilePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateProfilePostCall(UpdateProfileRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("data[f_name]", body.getData().getFName());
        formParams.put("data[l_name]", body.getData().getLName());
        formParams.put("data[email]", body.getData().getEmail());
        formParams.put("data[is_public]", body.getData().isIsPublic() ? 1 : 0);
        formParams.put("data[language]", body.getData().getLanguage());

        return buildCall(Command.UPDATE_PROFILE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Profile
     * @param body  (required)
     * @return GetProfileResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetProfileResponse getProfilePost(GetProfileRequest body) throws ApiException {
        ApiResponse<GetProfileResponse> resp = getProfilePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Profile
     * @param body  (required)
     * @return ApiResponse&lt;GetProfileResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetProfileResponse> getProfilePostWithHttpInfo(GetProfileRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getProfilePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetProfileResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Profile (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getProfilePostAsync(GetProfileRequest body, final ApiCallback<GetProfileResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getProfilePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetProfileResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getProfilePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getProfilePostCall(GetProfileRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());

        return buildCall(Command.GET_PROFILE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Order
     * @param body  (required)
     * @return UpdateOrderResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateOrderResponse updateOrderPost(UpdateOrderRequest body) throws ApiException {
        ApiResponse<UpdateOrderResponse> resp = updateOrderPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Order
     * @param body  (required)
     * @return ApiResponse&lt;UpdateOrderResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateOrderResponse> updateOrderPostWithHttpInfo(UpdateOrderRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateOrderPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateOrderResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Order (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateOrderPostAsync(UpdateOrderRequest body, final ApiCallback<UpdateOrderResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateOrderPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateOrderResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateOrderPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateOrderPostCall(UpdateOrderRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        for (UpdateOrderRequest.FolderOrder folderOrder: body.getFolders()) {
            formParams.put(String.format("folder[%d]", folderOrder.getId()), folderOrder.getOrderId());
        }

        return buildCall(Command.UPDATE_ORDER, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Recover File
     * @param body  (required)
     * @return RecoverFileResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public RecoverFileResponse recoverFilePost(RecoverFileRequest body) throws ApiException {
        ApiResponse<RecoverFileResponse> resp = recoverFilePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Recover File
     * @param body  (required)
     * @return ApiResponse&lt;RecoverFileResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<RecoverFileResponse> recoverFilePostWithHttpInfo(RecoverFileRequest body) throws ApiException {
        com.squareup.okhttp.Call call = recoverFilePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<RecoverFileResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Recover File (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call recoverFilePostAsync(RecoverFileRequest body, final ApiCallback<RecoverFileResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = recoverFilePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<RecoverFileResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for recoverFilePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call recoverFilePostCall(RecoverFileRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("id", body.getId());
        formParams.put("folder_id", body.getFolderId());

        return buildCall(Command.RECOVER_FILE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Settings
     * @param body  (required)
     * @return UpdateSettingsResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateSettingsResponse updateSettingsPost(UpdateSettingsRequest body) throws ApiException {
        ApiResponse<UpdateSettingsResponse> resp = updateSettingsPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Settings
     * @param body  (required)
     * @return ApiResponse&lt;UpdateSettingsResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateSettingsResponse> updateSettingsPostWithHttpInfo(UpdateSettingsRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateSettingsPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateSettingsResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Settings (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateSettingsPostAsync(UpdateSettingsRequest body, final ApiCallback<UpdateSettingsResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateSettingsPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateSettingsResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateSettingsPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateSettingsPostCall(UpdateSettingsRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("play_beep", body.getPlayBeep().getValue());
        formParams.put("files_permission", body.getFilesPermission().getValue());

        return buildCall(Command.UPDATE_SETTINGS, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Settings
     * @param body  (required)
     * @return GetSettingsResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetSettingsResponse getSettingsPost(GetSettingsRequest body) throws ApiException {
        ApiResponse<GetSettingsResponse> resp = getSettingsPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Settings
     * @param body  (required)
     * @return ApiResponse&lt;GetSettingsResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetSettingsResponse> getSettingsPostWithHttpInfo(GetSettingsRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getSettingsPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetSettingsResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Settings (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getSettingsPostAsync(GetSettingsRequest body, final ApiCallback<GetSettingsResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getSettingsPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetSettingsResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getSettingsPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getSettingsPostCall(GetSettingsRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());

        return buildCall(Command.GET_SETTINGS, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Messages
     * @param body  (required)
     * @return GetMessagesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetMessagesResponse getMessagesPost(GetMessagesRequest body) throws ApiException {
        ApiResponse<GetMessagesResponse> resp = getMessagesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Messages
     * @param body  (required)
     * @return ApiResponse&lt;GetMessagesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetMessagesResponse> getMessagesPostWithHttpInfo(GetMessagesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getMessagesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetMessagesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Messages (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getMessagesPostAsync(GetMessagesRequest body, final ApiCallback<GetMessagesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getMessagesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetMessagesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getMessagesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getMessagesPostCall(GetMessagesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());

        return buildCall(Command.GET_MESSAGES, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Buy Credits
     * @param body  (required)
     * @return BuyCreditsResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public BuyCreditsResponse buyCreditsPost(BuyCreditsRequest body) throws ApiException {
        ApiResponse<BuyCreditsResponse> resp = buyCreditsPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Buy Credits
     * @param body  (required)
     * @return ApiResponse&lt;BuyCreditsResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<BuyCreditsResponse> buyCreditsPostWithHttpInfo(BuyCreditsRequest body) throws ApiException {
        com.squareup.okhttp.Call call = buyCreditsPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<BuyCreditsResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Buy Credits (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call buyCreditsPostAsync(BuyCreditsRequest body, final ApiCallback<BuyCreditsResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = buyCreditsPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<BuyCreditsResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for buyCreditsPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call buyCreditsPostCall(BuyCreditsRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("amount", body.getAmount());
        formParams.put("receipt", body.getReceipt());
        formParams.put("product_id", body.getProductId());
        formParams.put("device_type", body.getDeviceType().getValue());

        return buildCall(Command.BUY_CREDITS, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update Device Token
     * @param body  (required)
     * @return UpdateDeviceTokenResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateDeviceTokenResponse updateDeviceTokenPost(UpdateDeviceTokenRequest body) throws ApiException {
        ApiResponse<UpdateDeviceTokenResponse> resp = updateDeviceTokenPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update Device Token
     * @param body  (required)
     * @return ApiResponse&lt;UpdateDeviceTokenResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateDeviceTokenResponse> updateDeviceTokenPostWithHttpInfo(UpdateDeviceTokenRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateDeviceTokenPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateDeviceTokenResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update Device Token (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateDeviceTokenPostAsync(UpdateDeviceTokenRequest body, final ApiCallback<UpdateDeviceTokenResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateDeviceTokenPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateDeviceTokenResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateDeviceTokenPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateDeviceTokenPostCall(UpdateDeviceTokenRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("device_token", body.getDeviceToken());
        formParams.put("device_type", body.getDeviceType().getValue());

        return buildCall(Command.UPDATE_DEVICE_TOKEN, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Translations
     * @param body  (required)
     * @return GetTranslationsResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetTranslationsResponse getTranslationsPost(GetTranslationsRequest body) throws ApiException {
        ApiResponse<GetTranslationsResponse> resp = getTranslationsPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Translations
     * @param body  (required)
     * @return ApiResponse&lt;GetTranslationsResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetTranslationsResponse> getTranslationsPostWithHttpInfo(GetTranslationsRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getTranslationsPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetTranslationsResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Translations (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getTranslationsPostAsync(GetTranslationsRequest body, final ApiCallback<GetTranslationsResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getTranslationsPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetTranslationsResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getTranslationsPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getTranslationsPostCall(GetTranslationsRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("language", body.getLanguage());

        return buildCall(Command.GET_TRANSLATIONS, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Languages
     * @param body  (required)
     * @return GetLanguagesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetLanguagesResponse getLanguagesPost(GetLanguagesRequest body) throws ApiException {
        ApiResponse<GetLanguagesResponse> resp = getLanguagesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Languages
     * @param body  (required)
     * @return ApiResponse&lt;GetLanguagesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetLanguagesResponse> getLanguagesPostWithHttpInfo(GetLanguagesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getLanguagesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetLanguagesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Languages (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getLanguagesPostAsync(GetLanguagesRequest body, final ApiCallback<GetLanguagesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getLanguagesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetLanguagesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getLanguagesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getLanguagesPostCall(GetLanguagesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());

        return buildCall(Command.GET_LANGUAGES, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Phones
     * @param body  (required)
     * @return GetPhonesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetPhonesResponse getPhonesPost(GetPhonesRequest body) throws ApiException {
        ApiResponse<GetPhonesResponse> resp = getPhonesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Phones
     * @param body  (required)
     * @return ApiResponse&lt;GetPhonesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetPhonesResponse> getPhonesPostWithHttpInfo(GetPhonesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getPhonesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetPhonesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Phones (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getPhonesPostAsync(GetPhonesRequest body, final ApiCallback<GetPhonesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getPhonesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetPhonesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getPhonesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getPhonesPostCall(GetPhonesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());

        return buildCall(Command.GET_PHONES, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Update User
     * @param body  (required)
     * @return UpdateUserResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UpdateUserResponse updateUserPost(UpdateUserRequest body) throws ApiException {
        ApiResponse<UpdateUserResponse> resp = updateUserPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Update User
     * @param body  (required)
     * @return ApiResponse&lt;UpdateUserResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UpdateUserResponse> updateUserPostWithHttpInfo(UpdateUserRequest body) throws ApiException {
        com.squareup.okhttp.Call call = updateUserPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UpdateUserResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Update User (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call updateUserPostAsync(UpdateUserRequest body, final ApiCallback<UpdateUserResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = updateUserPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UpdateUserResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for updateUserPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call updateUserPostCall(UpdateUserRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("app", body.getApp().getValue());
        formParams.put("time_zone", body.getTimezone());

        return buildCall(Command.UPDATE_USER, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Notify User
     * @param body  (required)
     * @return NotifyUserResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public NotifyUserResponse notifyUserPost(NotifyUserRequest body) throws ApiException {
        ApiResponse<NotifyUserResponse> resp = notifyUserPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Notify User
     * @param body  (required)
     * @return ApiResponse&lt;NotifyUserResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<NotifyUserResponse> notifyUserPostWithHttpInfo(NotifyUserRequest body) throws ApiException {
        com.squareup.okhttp.Call call = notifyUserPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<NotifyUserResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Notify User (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call notifyUserPostAsync(NotifyUserRequest body, final ApiCallback<NotifyUserResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = notifyUserPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<NotifyUserResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for notifyUserPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call notifyUserPostCall(NotifyUserRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("title", body.getTitle());
        formParams.put("body", body.getBody());
        formParams.put("device_type", body.getDeviceType().getValue());

        return buildCall(Command.NOTIFY_USER, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Upload Meta File
     * @param body  (required)
     * @return UploadMetaFileResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public UploadMetaFileResponse uploadMetaFilePost(UploadMetaFileRequest body) throws ApiException {
        ApiResponse<UploadMetaFileResponse> resp = uploadMetaFilePostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Upload Meta File
     * @param body  (required)
     * @return ApiResponse&lt;UploadMetaFileResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<UploadMetaFileResponse> uploadMetaFilePostWithHttpInfo(UploadMetaFileRequest body) throws ApiException {
        com.squareup.okhttp.Call call = uploadMetaFilePostCall(body, null, null);
        Type localVarReturnType = new TypeToken<UploadMetaFileResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Upload Meta File (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call uploadMetaFilePostAsync(UploadMetaFileRequest body, final ApiCallback<UploadMetaFileResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = uploadMetaFilePostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<UploadMetaFileResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for uploadMetaFilePost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call uploadMetaFilePostCall(UploadMetaFileRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("file", body.getFile());
        formParams.put("name", body.getName());
        formParams.put("parent_id", body.getParentId());
        if (body.getId() != null) {
            formParams.put("id", body.getId());
        }

        return buildCall(Command.UPLOAD_META_FILE, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Get Meta Files
     * @param body  (required)
     * @return GetMetaFilesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public GetMetaFilesResponse getMetaFilesPost(GetMetaFilesRequest body) throws ApiException {
        ApiResponse<GetMetaFilesResponse> resp = getMetaFilesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Get Meta Files
     * @param body  (required)
     * @return ApiResponse&lt;GetMetaFilesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<GetMetaFilesResponse> getMetaFilesPostWithHttpInfo(GetMetaFilesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = getMetaFilesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<GetMetaFilesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get Meta Files (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call getMetaFilesPostAsync(GetMetaFilesRequest body, final ApiCallback<GetMetaFilesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = getMetaFilesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<GetMetaFilesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for getMetaFilesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call getMetaFilesPostCall(GetMetaFilesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("parent_id", body.getParentId());

        return buildCall(Command.GET_META_FILES, formParams, progressListener, progressRequestListener);
    }

    /**
     *
     * Delete Meta Files
     * @param body  (required)
     * @return DeleteMetaFilesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public DeleteMetaFilesResponse deleteMetaFilesPost(DeleteMetaFilesRequest body) throws ApiException {
        ApiResponse<DeleteMetaFilesResponse> resp = deleteMetaFilesPostWithHttpInfo(body);
        return resp.getData();
    }

    /**
     *
     * Delete Meta Files
     * @param body  (required)
     * @return ApiResponse&lt;DeleteMetaFilesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<DeleteMetaFilesResponse> deleteMetaFilesPostWithHttpInfo(DeleteMetaFilesRequest body) throws ApiException {
        com.squareup.okhttp.Call call = deleteMetaFilesPostCall(body, null, null);
        Type localVarReturnType = new TypeToken<DeleteMetaFilesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete Meta Files (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call deleteMetaFilesPostAsync(DeleteMetaFilesRequest body, final ApiCallback<DeleteMetaFilesResponse> callback) throws ApiException {
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = deleteMetaFilesPostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<DeleteMetaFilesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for deleteMetaFilesPost
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call deleteMetaFilesPostCall(DeleteMetaFilesRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        if (body.getIds() != null) {
            formParams.put("ids", body.getIds());
        }
        if (body.getParentId() != null) {
            formParams.put("parent_id", body.getParentId());
        }

        return buildCall(Command.DELETE_META_FILES, formParams, progressListener, progressRequestListener);
    }

    private Call buildCall(
            Command command,
            Map<String, Object> formParams,
            ProgressResponseBody.ProgressListener progressListener,
            ProgressRequestBody.ProgressRequestListener progressRequestListener
    ) throws ApiException {
        final String[] localVarAccepts = {
                "application/json"
        };
        Map<String, String> localVarHeaderParams = new HashMap<>();
        String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        String[] localVarContentTypes = { command.contentType };
        String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                        .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                        .build();
            });
        }

        return apiClient.buildCall(
                command.name,
                "POST",
                Collections.emptyList(),
                Collections.emptyList(),
                null,
                localVarHeaderParams,
                formParams,
                new String[0],
                progressRequestListener
        );
    }
    
    private static Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> createProgressListeners(ApiCallback<?> callback) {
        return callback != null
                ? Pair.of(callback::onDownloadProgress, callback::onUploadProgress)
                : Pair.of(null, null);
    }

}
