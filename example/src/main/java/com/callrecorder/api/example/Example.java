package com.callrecorder.api.example;

import com.callrecorder.api.CallRecorderApi;
import com.callrecorder.api.CallRecorderClientFactory;
import com.callrecorder.api.client.ApiException;
import com.callrecorder.api.dto.RegisterPhoneRequest;
import com.callrecorder.api.dto.RegisterPhoneResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Example {

    private static Logger log = LoggerFactory.getLogger(Example.class);

    public static void main(String[] args) throws ApiException {
        CallRecorderApi apiClient = CallRecorderClientFactory.createApiClient("https://app2.virtualbrix.net");

        RegisterPhoneResponse response = apiClient.registerPhonePost(
                new RegisterPhoneRequest()
                        .token("55942ee3894f51000530894")
                        .phone("+16463742122")
        );
        if (!"ok".equals(response.getStatus())) {
            log.error("Failed to register phone: response={}", response);
            return;
        }

        String code = response.getCode();
        log.info("Verification code was received successfully: code={}", code);
    }

}
