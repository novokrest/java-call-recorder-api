from sys import argv

PATTERN = """
  /{name}:
    post:
      description: |
        {title}
      requestBody:
        $ref: '#/components/requestBodies/{pascal_name}Request'
      responses:
        '200':
          $ref: '#/components/responses/{pascal_name}Response'

    {pascal_name}Request:
      required: true
      content:
        application/x-www-form-urlencoded:
          schema:
            $ref: '#/components/schemas/{pascal_name}Request'

    {pascal_name}Response:
      description: ok
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/{pascal_name}Response'

    {pascal_name}Request:
      type: object
      properties:
        api_key:
          type: string
          example: '57872b508520557872b50855c'
      required:
        - api_key

    {pascal_name}Response:
      type: object
      properties:
        status:
          type: string
          example: 'ok'
        msg:
          type: string
          example: 'Successfully'
      required:
        - status

=============================================
    /**
     *
     * {title}
     * @param body  (required)
     * @return {pascal_name}Response
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public {pascal_name}Response {camel_name}Post({pascal_name}Request body) throws ApiException {{
        ApiResponse<{pascal_name}Response> resp = {camel_name}PostWithHttpInfo(body);
        return resp.getData();
    }}

    /**
     *
     * {title}
     * @param body  (required)
     * @return ApiResponse&lt;{pascal_name}Response&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body

     */
    public ApiResponse<{pascal_name}Response> {camel_name}PostWithHttpInfo({pascal_name}Request body) throws ApiException {{
        com.squareup.okhttp.Call call = {camel_name}PostCall(body, null, null);
        Type localVarReturnType = new TypeToken<{pascal_name}Response>(){{}}.getType();
        return apiClient.execute(call, localVarReturnType);
    }}

    /**
     * {title} (asynchronously)
     * @param body  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object

     */
    public com.squareup.okhttp.Call {camel_name}PostAsync({pascal_name}Request body, final ApiCallback<{pascal_name}Response> callback) throws ApiException {{
        Pair<ProgressRequestBody.ProgressRequestListener, ProgressResponseBody.ProgressListener> listeners = createProgressListeners(callback);
        com.squareup.okhttp.Call call = {camel_name}PostCall(body, listeners.getRight(), listeners.getLeft());
        Type localVarReturnType = new TypeToken<{pascal_name}Response>(){{}}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }}

    /**
     * Build call for {camel_name}Post
     * @param body  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object

     */
    public com.squareup.okhttp.Call {camel_name}PostCall({pascal_name}Request body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {{
        requireNonNull(body, "body");

        Map<String, Object> formParams = new HashMap<>();
        formParams.put("api_key", body.getApiKey());
        formParams.put("", );

        return buildCall(Command.{upper_case_snake_name}, formParams, progressListener, progressRequestListener);
    }}
"""

def from_upper(str):
    return str[0].upper() + str[1:]

def from_lower(str):
    return str[0].lower() + str[1:]

def to_handle_title(name):
    return ' '.join([from_upper(part) for part in name.split('_')])

def to_pascal(name):
    return ''.join([from_upper(part) for part in name.split('_')])

def to_camel(name):
    return from_lower(to_pascal(name))

if __name__ == '__main__':
    if len(argv) < 2:
        print('Few arguments! Specify handle name')
        print(f'Use: {argv[0]} create_user')
        exit(1)
    handle_name = argv[1]
    result = PATTERN.format(
        name=handle_name, 
        title=to_handle_title(handle_name), 
        pascal_name=to_pascal(handle_name),
        camel_name=to_camel(handle_name),
        upper_case_snake_name=handle_name.upper()
    )
    print(result)