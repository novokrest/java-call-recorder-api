# call-recorder-api

Java library to use [Call Recorder API](docs/api.pdf)

### Build
```bash
./gradlew clean -x test build
```

### Usage
```java
CallRecorderApi apiClient = CallRecorderClientFactory.createApiClient("https://app2.virtualbrix.net");

RegisterPhoneResponse response = apiClient.registerPhonePost(
        new RegisterPhoneRequest()
                .token("55942ee3894f51000530894")
                .phone("+16463742122")
);

if ("ok".equals(response.getStatus())) {
    String code = response.getCode();
    log.info("Verification code was received successfully: code={}", code);
} else {
    log.error("Failed to register phone: response={}", response);
}
```

### Example
There is [example project](example) using this library